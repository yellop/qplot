from collections import OrderedDict
from copy import copy
from argparse import ArgumentParser
import re, os
from cut.mixedtools import get_args, iscontainer, is_string_like, type_down, to_yaml_safe
from cut.fileaccess import save_file
import argparse


class Argument():
    
    
    ## TODO: effective value
    def __init__(self,
             main_key   =   None,
             side_keys  =   [],
             subcmds    =   None,
             default    =   None,
             completion_func = None,
             properties =   None,
             help_text  =   None,
             
             ## for completion purpose
             used_key   = None,
             status     = None,
             values     = [],
             value      = None, ## last of values
             subcmd     = None,
             ):
        


        self.main_key   = main_key
        self.side_keys  = side_keys
        self.subcmds    = subcmds
        self.default    = default
        self.completion_func = completion_func
        self.properties = properties
        self.help_text  = help_text
        ## for completion purpose
        self.used_key   = used_key
        self.status     = status
        self.values     = values
        self.value      = value
        self.subcmd     = subcmd


    def get_n_values(self):
        n_values = None
        if 'nargs' in self.properties:
            n_values_raw = self.properties['nargs']
            n_values = None
            
            try:
                n_values = int(n_values_raw)
            except:
                if n_values_raw in ['*','+']:
                    n_values = n_values_raw
                    
        else:
            if 'action' in self.properties:
                if self.properties['action'] in ['store_constant',
                                                 'store_true',
                                                 'store_false']:
                    n_values = 0
                else:
                    n_values = 1
            else:
                n_values = 1
        
        #print(self.main_key + str(n_values))
        return n_values

    def get_value(self):
        arg         = self
        arg_value   = arg.value
        arg_default = arg.properties.get('default', None)
        if arg_value == None and arg_default != None:
            value = arg_default
        else:
            value = arg_value
        return value



class CmdLineArgs():
    
    def __init__(self,
             cmdline = None,
             use_subcmds=True):
         
        self.cmdline = cmdline
        self.use_subcmds = use_subcmds
        ## any arguments per main-key  
        self.arguments       = OrderedDict()
        
        ## arguments per subcmd
        self.bound_arguments = OrderedDict()
        
        ## 
        self.subcmd_parsers  = OrderedDict()
       
        
    def parse_args(self, args_raw):
        
        args = self.subcmd_parsers[''].parse_args(args_raw)
        return args
    
    
    
    def create_root_parser(self,
                           prog,
                           description,
                           ):
        
        self.subcmd_parsers[''] = \
        main_parser = ArgumentParser(description=description,
                                     prog=prog,
                                     #func=self.cmdline._main
                                     )    
         
        ## TODO:
        ## is this a problem for argparse?
        ## if so we have to keep subparser in separate container
        if self.use_subcmds:
            main_parser.subparsers  = main_parser.add_subparsers( 
                                                dest='subcmd',
                                                help='sub-command help')


    

    def define_argument(self, *keys, **properties):
          
        assert keys
        
        ## TODO: aehaem
        properties_, subcmds, default_value, completion_func, \
        help_text = \
            self.strip_properties(properties)
        
        main_key = keys[0]
        
        argument = Argument(main_key    = main_key,
                            side_keys   = list(keys[1:]),
                            subcmds     = subcmds,
                            properties  = properties_,
                            default     = default_value,
                            help_text   = help_text,
                            completion_func = completion_func )
                            
        
        if subcmds is None:
            subcmds = [None]
        assert subcmds and iscontainer(subcmds)

        if not main_key in self.arguments:
            self.arguments[main_key] = OrderedDict()
        
        for subcmd in subcmds:
            self.arguments[main_key][subcmd]  = argument

    def update_argument(self, *keys, **properties):
        
        main_key    = keys[0]
        side_keys   = list(keys[1:])
        properties_, subcmds, default_value, completion_func, \
        help_text = \
            self.strip_properties(properties)
        
        if subcmds is None:
            subcmds = [None]
        assert subcmds and iscontainer(subcmds)
        
        for subcmd in subcmds:
        
            argument = self.get_argument(main_key, subcmd)
            
            argument.side_keys  = side_keys
            if subcmds is not None:
                argument.subcmds    = subcmds
            if default_value is not None:
                argument.default    = default_value
            if completion_func is not None:
                argument.completion_func = completion_func
            if help_text is not None:
                argument.help_text = help_text 
            
            for k,v in properties_.items():
                argument.properties[k] = v    
        


    def strip_properties(self, properties):
        properties_ = copy(properties)
        
        subcmds     = None
        if 'subcmds' in properties_:
            subcmds = properties_.pop('subcmds')
        
        completion_func = None
        if 'completion_func' in properties_:
            completion_func = properties_.pop('completion_func')
        
        
        default_value = None
        
        if 'action' in properties_:

            action = properties_['action']
            if action == 'store_true':
                default_value = False
            if action == 'store_false':
                default_value = True
        #else:
        #    properties_['action'] = 'store'
            
        if 'default' in properties_:
            default_value = properties_['default']
        
        ## 
        help_text = None
        if 'help' in properties_:
            if not properties_['help'] == argparse.SUPPRESS:
                help_text = properties_['help']
            
        ## TODO: aehem
        return  properties_, subcmds, default_value, completion_func, help_text
        
        

   
    def get_argument(self, 
                     main_key, 
                     subcmd=None):
        argument = None
        if main_key in self.arguments:
            if subcmd in self.arguments[main_key]:
                argument = self.arguments[main_key][subcmd]
                return argument
            if None in self.arguments[main_key]:
                argument = self.arguments[main_key][None]

            #show(argument)
            #if subcmd in argument.subcmds is None or subcmdset(argument.subcmds)):
            #    return argument
        if argument is None:
            raise Exception("cannot find argument %s for subcmd '%s'" % \
                                (main_key, subcmd)
                       )
        else:
            return argument
    
    def get_bound_argument(self, 
                           subcmd,
                           main_key,
                           ):
        if subcmd in self.bound_arguments:
            if main_key in self.bound_arguments[subcmd]:
                return self.bound_arguments[subcmd][main_key] 
        return None


    def get_bound_main_keys(self, 
                            subcmd,
                           ):
        if subcmd in self.bound_arguments:
            return self.bound_arguments[subcmd].keys() 
        
        return []

    
    def get_bound_main_key(self,
                           subcmd,
                           key):
        for main_key, argument in self.bound_arguments[subcmd].items():
            assert main_key == argument.main_key
            keys = [argument.main_key] + argument.side_keys
            if key in keys:
                return argument.main_key
        return None 
            
    @staticmethod
    def args_to_dict(args):
        return type_down(args)
    
    

    def add_subcmd(self,
                   subcmd,
                   main_keys=[], 
                   help=None,
                   func=None):
        

        parser = self.subcmd_parsers[''].subparsers.add_parser(subcmd, help=help)
        parser.set_defaults(func=func)
        
        self.subcmd_parsers[subcmd] = parser
        
        self.add_arguments(subcmd, main_keys)            
        
        return parser

    def add_arguments(self, 
                      subcmd,
                      main_keys):
        
        parser = self.subcmd_parsers[subcmd]  
        
        if not subcmd in self.bound_arguments:
                self.bound_arguments[subcmd] = OrderedDict()
        
        for main_key in main_keys:

            argument = self.get_argument(main_key, subcmd)
            
            properties = copy(argument.properties)
            
            if 'subcmds' in properties:
                properties.pop('subcmds')
            if 'completion_func' in properties:
                properties.pop('completion_func')
            
            ##TODO:
            #print("")
            #print(argument.main_key)
            #print(argument.side_keys)
            
            #print(properties)
            
            
            
            parser.add_argument(argument.main_key, 
                                *argument.side_keys,
                                **properties)
            
            
            
            self.bound_arguments[subcmd][main_key]= argument
            

    ## TODO: hide subcmd-wise 
    def hide_arguments(self, *main_keys):
        for main_key in main_keys:
            argument = self.get_argument(main_key)
            #pass
            d = {'help': argparse.SUPPRESS}
            self.update_argument(main_key,
                                 *argument.side_keys,
                                 **d )
            

   
        
    def get_subcmd_positionals(self, subcmd):
        
        pass        

    def get_subcommands(self):
        subcmds = list(self.subcmd_parsers.keys())[:]
        if '' in subcmds:
            subcmds.remove('')
        return subcmds
        
        
        
    def get_options(self,subcmd=''):
                
        arguments = self.bound_arguments[subcmd].keys()
            
        options   = [ a for a in arguments if re.match('--', a) ] 

        return options
        #return self.cla.subcmd_parsers['']._optionals._option_string_actions.keys()
        #sub_parser = self.cla.subcmd_parsers[subcmd].choices[subcmd]
        #return sub_parser._optionals._option_string_actions.keys()
        

    def get_positional_args(self,subcmd=''):
        
        
        arguments = self.bound_arguments[subcmd].keys()
        
        positionals   = [ a for a in arguments if not re.match('--', a) ] 

        return positionals
      
    
    
    
### completion

    def apply_completion_func(self, 
                              argument, 
                              comletion_prefix, 
                              arguments,
                              arguments_main):
        """Passes its args to completion-func cf of argument.
        It finds out how many args cf takes (1, 2, or 3) and passed that many
        in the above order truncating from tail. 
        """
        cf = argument.completion_func
        mandatory_names = get_args(cf)[0]
        n_args          = len(mandatory_names)
        
        all_inputs      = [argument, comletion_prefix, arguments, arguments_main]
        
        if hasattr(cf, '__self__') and cf.__self__:  
            out =   cf(*all_inputs[:n_args])
        else:
            out =   cf(self.cmdline, *all_inputs[:n_args])
            
        if out is None:
            out = []
        return out
            


    def complete(self, context, completion_prefix=''):
        
        #self._save_values("AAA "+ str(context), append=False)
            
        
        if is_string_like(context):
            context = context.split()
        
        out = []
        if not '--help' in context:
            out = ['--help']
                    
        main_context, subcmd, subcontext = self.extract_subcmd_subcontext(context)
        
        
        self.digest_context('', main_context)
        
        self.digest_context(subcmd, subcontext)
        

        arguments        = self.bound_arguments[subcmd]
        
        arguments_main   = self.bound_arguments['']
        #save_file(to_yaml_safe(arguments_main), '/tmp/compl_02', append=True)
        
        ## do we have an open argument? 
        ## which arguments are unseen?
        argument_open    = None
        arguments_unseen = []    
        ## TODO: we should iterate in order of appearance
        ## to make last open arg to be argument_open
        for a in arguments.values():  
            
            #self._save_values("BBB "+a.main_key,)
            #self._save_values("BB1 "+a.status,  )
            

            if a.status == 'open':
                if not argument_open:
                    argument_open = a
            if a.status == 'unseen':
                arguments_unseen += [ a ]
        
        self._save_values({'argument_open': argument_open,
                           'arguments_unseen': arguments_unseen,
                           }
                           )
        #self._save_values("BB1 "+a.status,  )
       
        ## if there we have argument_open go for its completion 
        if argument_open:
            ## we use both completion_func and choices
            
            if argument_open.completion_func:
                
                out_new = self.apply_completion_func(
                                argument_open, 
                                completion_prefix, 
                                arguments, 
                                arguments_main)
                
                #self._save_values({'out_new': out_new})
            
                out += out_new
            
            
            #print(argument_open.main_key)
            if 'choices' in argument_open.properties:
                out += argument_open.properties['choices']
            
        ## if argument_open is not options-arg
        ## (if its positional it might be open with nargs +/*
        if not (argument_open and argument_open.main_key.startswith('-')):
         
            if not subcmd:
                out += self.get_subcommands()
            
            for argument_unseen in arguments_unseen:
                main_key = argument_unseen.main_key
                if main_key.startswith('-'):
                    if argument_unseen.properties['help'] != argparse.SUPPRESS:
                        ## though bash-completion framework cares for  
                        ## prefix-filtering we do it here
                        if main_key.startswith(completion_prefix):
                            out += [ main_key ]         
                else:
                    if not argument_open:
                        if argument_unseen.completion_func:
                            out += self.apply_completion_func(
                                    argument_unseen, completion_prefix, 
                                    arguments, arguments_main)
                        ## TODO: arguments_unseen[0] only
                        break

        out = [str(x) for x in out]
        
        #print(to_yaml_safe(out))
        #quit()
            
        
        return out
        
                        
    
    def digest_context(self, subcmd, subcontext):
        """strip closed context parts"""
        
        ## reinit attributes 
        for argument in self.bound_arguments[subcmd].values():
            argument.status   = 'unseen'
            argument.subcmd   = subcmd
            ## no change to init
            argument.used_key = None
            argument.values   = []
            argument.value    = None
            #argument.final_value = None
            
        
        ##
        positionals = self.get_positional_args(subcmd) 
        argument    = None
        
        
        
        #self._save_values([subcontext], 'cc')
        
        first = True
        
        while first or subcontext:

            first = False
            
            word = None
            if subcontext:
                word = subcontext[0]
                        
            main_key = self.get_bound_main_key(subcmd, word)            
            
            if main_key:                
                ## option case
                used_key = subcontext.pop(0)
                assert used_key == word
            else:
                ## positional case
                if positionals: 
                    main_key = positionals.pop(0)
                else:
                    ## error, no positionals left
                    return None
                used_key = ''
             
            argument = self.bound_arguments[subcmd][main_key]
            argument.status     = 'open'
            argument.used_key   = used_key
            n_values = argument.get_n_values()
            
            #print("\nAAAA")
            #print(main_key)
            #print(argument.status)     
            #print(n_values)
            
            ## consume argument value/more values
            while n_values and subcontext:
                
                word_ = subcontext[0]
                #if 1 or word_:
                main_key_ = self.get_bound_main_key(subcmd, word_)
                if not main_key_:
                    word  = subcontext.pop(0)
                    argument.values.append(word)
                    argument.value = argument.values[-1]
                    argument.final_value = argument.value
                    if not n_values in ['+', '*']:
                        n_values -= 1                
                else:
                    argument.status = 'closed'
                    break
                
            #print(argument.status)                
            #print(n_values)
            
            if not n_values:
                argument.status = 'closed'

            #print("EEEE")
            #print(argument.value)
            ## TODO:
            ## problem: if arg is not in context we do not pass here
            #if argument.value == None:
            #    print("TTTTTT")
            #    print(argument.properties)
            #    argument.final_value = argument.properties.get('default', argument.value)
                    
            #print(argument.status)
            
            
            
                    
                    
        
    
    
   

    def extract_subcmd_subcontext(self, context):
        
        subcmds = self.get_subcommands()
        main_context = context[:]
        subcmd = ''
        subcontext = context[:]
        for i, word in enumerate(context):
            if word in subcmds:
                subcmd = word
                main_context = context[:i]
                subcontext = context[i+1:]
                break
            #else:
            #    subcontext = []
        
        #subcontext = [''] + subcontext
        
        return main_context, subcmd, subcontext
    
    
        
    def _save_values(self, values, filename='values', append=True):
        
        wdir = '/tmp/scmd_completion_trial/'
        if not os.path.isdir(wdir):
            os.makedirs(wdir)
        path_args = os.path.join(wdir, filename)
        if not append and os.path.isfile(path_args):
            os.remove(path_args)
        #if type(values) in [list,tuple]:
        #    values = str("\n".join([str(v) for v in values]))
        save_file('\n' + to_yaml_safe(values), path_args, append=append)



  
    
