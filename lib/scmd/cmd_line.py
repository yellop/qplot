"""
cares for
- simpler arg-definition
- completion
- logging
"""

import os, sys, getpass, signal, tempfile, re
from cut.log import Logging
from abc import abstractmethod
from collections import OrderedDict
from cut.fileaccess import save_file, homedir, remove
from cut.mixedtools import to_yaml_safe, from_yaml_file, to_yaml, iscontainer,\
                           is_string_like
import shutil
from scmd.cla import CmdLineArgs 
import glob
import inspect


class CmdLine():
    
    def __init__(self,
                prog         = 'scmd',
                description  = None,
                version      = '0.1',
                user         = None,
                
                parser_funcs = ['build_parser_base',
                                'build_parser',
                                'build_parser_main_args'],

                base_dir_distance = 2, 
                use_subcmds       = True,
                
                log_func     = None,
                logger       = None,
                logbuffer    = [],
                log_filename = 'scmd_log',
                log_level    = 'debug',
                log_path     = None,
                
                wdir_parent  = None,
                wdir         = None,
                
                keep_working_dir   = False,
                verboseness        = 'warning',
                clear_working_dir  = False,
                
                log_stdout_prefix  = '## ',
                log_user           = False,
                ):
            

        self.prog         =  prog
        self.description  = description
        self.version      = version
        self.user         = user
        
        self.parser_funcs = parser_funcs
        
        
        self.base_dir_distance = base_dir_distance 
        self.use_subcmds       = use_subcmds
        
        self.log_func     = log_func
        self.logger       = logger
        self.logbuffer    = logbuffer
        self.log_filename = log_filename 
        self.log_level    = log_level
        self.log_path     = log_path
        
        self.wdir_parent  = wdir_parent
        self.wdir         = wdir
        
        self.keep_working_dir   = keep_working_dir
        self.verboseness        = verboseness
        
        self.clear_working_dir  = clear_working_dir
        self.log_stdout_prefix  = log_stdout_prefix
        self.log_user           = log_user

        
        self.set_base_dir()
        
        
        if user is None:
            self.user  = self.get_user()
        
        
        ## preliminary wdir
        if not wdir_parent:
            self.wdir_parent = os.path.join('/tmp', self.prog)
        if not wdir:
            wdir_prefix = os.path.join(self.wdir_parent, 'wdir_')    
            self.wdir   = tempfile.mktemp(prefix=wdir_prefix)
        
        ## start logging in preliminary wdir
        if log_func:
            self.log = log_func
        else:
            if not log_path:
                self.log_filename = "%s_log" % prog
                log_path     = os.path.join(self.wdir, self.log_filename)
            self.logger = Logging(name=self.prog, 
                                      user=self.user,
                                      log_stdout_prefix = log_stdout_prefix, 
                                      logfile_path = log_path,
                                      verboseness  = verboseness,
                                      log_level    = log_level)
        
        self.cla = CmdLineArgs(cmdline=self,
                               use_subcmds=use_subcmds)    
        
        
 



### argument parsing


    def build_parser_base(self):
        
        self.cla.create_root_parser(self.prog,
                                    self.description,
                                    )
        
        
        self.define_argument('--version', 
                             action='version', 
                             version=self.version,
                             help='show version and exit')
        
        self.define_argument('--reveal-hidden-arguments', 
                             action="store_true",
                             help='show hidden arguments/options in help',
                             )

        
        self.define_argument('--verboseness', 
                             action="store",
                             default=self.verboseness, 
                             help='how much (non-value) output is shown, specified as loglevel',
                             choices = list(Logging.log_levels.keys()))

        self.define_argument('--parse-cmdline-only',
                             action="store_true",
                             help='parse command line, show result, and exit')
        
        
        self.define_argument('--user',  
                             action='store', 
                             help='set user')
        
        self.define_argument('--completion-call', 
                             action="store_true",
                             help='show possible completions')

        self.define_argument('--completion-prefix', 
                             help='explicitly provide prefix of argument to be completed')


        self.define_argument('--working-dir-parent',  
                             help='parent dir of workding dir, will be created if not present')

        
        self.define_argument('--working-dir',  
                             help='working dir, will be created if not present')

        
        self.define_argument('--keep-working-dir',  
                             action="store_true",
                             help='do not throw working-dir on exit, ' +\
                                  'w-dirs not below /tmp are always kept')

        self.define_argument('--dont-clean-working-dir',  
                             action="store_true",
                             help='do not remove working dir content at start')



    @abstractmethod
    def build_parser(self):
        pass


    def build_parser_main_args(self):
        
        self.add_arguments_main('--version',
				                '--verboseness' ,
							    '--parse-cmdline-only' ,
							    '--user' ,
								'--completion-call',
                                '--completion-prefix',
								'--keep-working-dir' ,
								'--working-dir-parent' ,
								'--working-dir',
                                '--dont-clean-working-dir' )
    

    def hide_arguments(self, *keys):
        
        
        if not '--reveal-hidden-arguments' in self.args_raw:        
            self.cla.hide_arguments(*keys)        
                
            

  

    
    def define_argument(self, *keys, **properties):
         
        self.cla.define_argument(*keys, **properties)  
        
    def update_argument(self, *keys, **properties):
        
        self.cla.update_argument(*keys, **properties)  
           
    def add_arguments_main(self, *argument_main_keys):
        self.cla.add_arguments('', argument_main_keys)
    
    
    def add_subcmd(self,
                   subcmd,
                   arguments=[], 
                   help=None,
                   func=None):

        self.cla.add_subcmd(subcmd=subcmd, 
                            main_keys=arguments, 
                            help=help, 
                            func=func)
        

### main   

    def _save_args(self, args_raw):
        wdir = '/tmp/scmd_completion_trial/'
        if not os.path.isdir(wdir):
            os.makedirs(wdir)
        path_args = os.path.join(wdir, 'arguments')
        if os.path.isfile(path_args):
            os.remove(path_args)
        save_file(str(args_raw), path_args )


 
    def main(self,args_raw=None):
        
        self.args_raw = args_raw
        
   
        ##
        if args_raw is None:
            args_raw = sys.argv[1:]
        else:
            if is_string_like(args_raw):
                args_raw  = args_raw.split()
        self.args_raw = args_raw
        
        #self._save_args(args_raw)
        
        ##
        self.log("call: " + ' '.join(self.args_raw), 'debug')
        
        
        
        #save_file('uhua', os.path.join(self.wdir, 'jij'))
        
        
        
        ## build parser   
        for parser_func_name in self.parser_funcs:
            getattr(self, parser_func_name)()

        ## completion
        completion = self.apply_completion(args_raw)
        if not completion is None:
            return completion
        
        ## parse     
        args      = \
        self.args = self.cla.parse_args(self.args_raw)
        if 'parse_cmdline_only' in args and args.parse_cmdline_only:
            print(to_yaml_safe(args))
            return args
            
        
        
        ## 
        self.apply_preset_args(args)
        
        ##
        if self.logger:
            self.logger.verboseness = args.verboseness
            self.logger.setup_handler_stdout()
        
        
        
        ##
        self.setup_wdir(args)
        
        if self.logger and not self.log_path:
            self.log_path = os.path.join(self.wdir, self.log_filename)
            self.logger.setup_handler_logfile(self.log_path)
        
        ##
        self.log('base-dir: %s' % self.base_dir)
        
        
        ## go
        self._main(args)
                      
        out =  self.run(args)
        self.remove_working_dir()            
        return out
        


    @abstractmethod
    def run(self, args):
        pass
    
    
    def _main(self, args):
        pass
    
    def apply_preset_args(self, args):
        
        ## TODO:
        ## get args from env
        ##     #if os.environ.has_key("CMDNAME_SUBCMD_ARG"):
        ##        self.target_host_default = os.environ["CMDNAME_SUBCMD_ARG"]
    
                
        ## TODO: work on copy off args in torder to make diffs
        #args2 = copy(args)
        
        
        ## set up ordered list of prog-dirs
        prog_dir_name = '.' + self.prog
        prog_dirs = [os.path.join(homedir(), prog_dir_name),
                     os.path.join(os.getcwd(), prog_dir_name),
                     ]
        
        ##
        cmd_call_name = os.path.basename(sys.argv[0])
        
        
        ##            
        def varname(argument_key):
            nopm = re.sub(r'^--', '', argument_key)
            nom  = re.sub(r'-', '_', nopm)
            return nom
        
        ##
        def merge_pre_args(subcmd, preset_args):
            for key in preset_args:
                key_main = self.cla.get_bound_main_key(subcmd,key)
                if key_main:
                    #argument = self.cla.bound_arguments[subcmd][key_main]
                    varname_arg = varname(key_main)
                    value = preset_args[key]
                    if iscontainer(value):
                        value = to_yaml(value)
                        
                    argsd = args.__dict__
                    ## TODO: None might be passed on cmd-line via yaml-arg
                    if varname_arg not in argsd or argsd[varname_arg] == None:
                        args.__setattr__(varname_arg, value)
                    
        ## go
        for prog_dir in prog_dirs:
            preset_args_path = os.path.join(prog_dir, 'preset_args')
            if os.path.isfile(preset_args_path):
                
                preset_args_root = from_yaml_file(preset_args_path)
                #show(preset_args_root)
                if cmd_call_name in preset_args_root:
                    preset_args = preset_args_root[cmd_call_name]
                    if preset_args:
                        ## args for main-cmds
                        merge_pre_args('', preset_args)
                        for subcmd in self.cla.get_subcommands():
                            if subcmd in preset_args:
                                preset_args_sub = preset_args[subcmd]
                                #show(preset_args_sub)
                                if preset_args_sub:
                                    ## args for subcmd                              
                                    merge_pre_args(subcmd, preset_args_sub)           
        
        #show(args)
        #quit()
    
    
        
                     
    def main_catched(self, arguments, output_mode='value'):
            
        m_tpl = 'call %s with arguments %s failed with exit-code %s'
            
        error = None
        value = None
        try:
            value = self.main(arguments)
        except Exception as e:
            error = e
            self.error(m_tpl % (self.prog, arguments, e))
            
        except BaseException as e:
            error = int(str(e))
            self.error(m_tpl % (self.prog, arguments, e))
        
        if output_mode == 'value':
            return value
        elif output_mode == 'error':
            return error
        else:
            raise Exception('inknown output_mode: %s' % output_mode)
        
        
    

### completion


    def apply_completion(self,arguments):
       
         
        if "--completion-call" in arguments:    
            context = arguments
            context.remove("--completion-call")
            
            completion_prefix = ''
            if '--completion-prefix' in arguments:
                key_index = context.index('--completion-prefix')
                context.pop(key_index)
                completion_prefix = context.pop(key_index)
                    
            list_base  = self.cla.complete(context, completion_prefix)
            #list_base  = [ e if not re.search("\s", e) else '"%s"' % e 
            #               for e in list_base ]
            out = "\n".join([str(x) for x in list_base])
            
            self.say(out)
            self.remove_working_dir()
            return out



    def _complete_path(self, _, prefix):  
        
        return self.complete_path(prefix,)
        

    def _complete_dir_path(self, _, prefix):  
        
        return self.complete_path(prefix,
                                  path_filter=os.path.isdir)
              





    def complete_path(self, prefix, path_filter=lambda x: True):
        
        
        def list_dir(path, prefix=''):
            try:
                dirnames = [ p for p in os.listdir(path) 
                             if p.startswith(prefix) and 
                                path_filter(os.path.join(path,p)) ]
            except OSError:
                dirnames = []
            return dirnames
        
        
        ## split prefix in dir-part and tail without sep
        sepe = re.escape(os.path.sep)
        if prefix in [os.path.curdir, os.path.pardir]:
            prefix_base_string  = prefix + os.path.sep
            prefix_tail         = ''
        elif os.path.sep in prefix:
            match = re.match(r'(.*%s)([^%s]*)$' % (sepe,sepe), prefix )
            ## till last / (including)
            prefix_base_string = match.group(1)
            ## rest
            prefix_tail        = match.group(2)
        else:
            prefix_base_string  = ''
            prefix_tail         = prefix
            
        
            
        prefix_base_dir = os.path.join(os.getcwd(), prefix_base_string)
             
        paths = list_dir(prefix_base_dir, prefix_tail)
        
        #print("AAA" + str(dirs) + "AAA")
        
        
        if len(paths) == 1:
            p     = paths[0]
            p_abs = os.path.join(prefix_base_string, p)
            if os.path.isdir(p_abs):
                ## add next level 
                ## otherwise bash-completion appends whitespace to proposal
                paths_ = [ os.path.join(p,d) 
                          for d in list_dir(p_abs) ]
                if paths_:
                    paths += paths_
                
                
        out = []
        for p in paths:
            p_abs = prefix_base_string + p
            if os.path.isdir(p_abs):
                if not p_abs.endswith(os.path.sep):
                    p_abs = p_abs + os.path.sep
            #p = prefix_base_string + d
            #p = d
            ## we would like p to show
            ## however bash-completion filters against prefix 
            ## so we have provide p_abs
            out.append(p_abs)
        
        return out 

        
        

        
        
### setup other services
    
     
          
    def get_user(self):
        
        if 'SUDO_USER' in os.environ and os.environ['SUDO_USER']:
            user = os.environ['SUDO_USER']
        else:
            user = getpass.getuser()
        
        return user

 
    def set_base_dir(self):
        """ment to be repo-base, often ../.."""
        
        try:
            ## in case we build a binary, we have a different base-dir
            root = sys._MEIPASS # @UndefinedVariable
            self.base_dir  = os.path.join(root, self.prog,)        
        except AttributeError:
                        
            module_abs    = inspect.getfile(self.__class__)
            package_dir   = os.path.dirname(module_abs)
            
            dist = self.base_dir_distance - 1
            cdir = package_dir
            while dist:
                cdir   = os.path.dirname(cdir)
                dist  -= 1            
            base_dir      = cdir
            self.base_dir = base_dir
        
        return self.base_dir



    def setup_wdir(self, args):

        
        wdir_pre = self.wdir
        wdir_pre = os.path.abspath(wdir_pre)
        wdir_pre = os.path.realpath(wdir_pre)


        
        if 'keep_working_dir' in args:
            self.keep_working_dir = args.keep_working_dir
        
        ##
        if 'working_dir_parent' in args and args.working_dir_parent:
            self.wdir_parent = args.working_dir_parent
            
        if self.wdir_parent and os.path.isfile(self.wdir_parent):
            self.error('working-dir-parent exists and its a file:\n%s' %\
                       self.wdir_parent, 11)
            
        if not os.path.exists(self.wdir_parent):
            os.makedirs(self.wdir_parent)
            os.chmod(self.wdir_parent, 0o777)
        
        
        #self.show_parse(args)
        ##
        if 'working_dir' in args and args.working_dir:
            self.log('wdir by args')
            self.wdir = args.working_dir
        else:
            if 'working_dir_parent' in args and args.working_dir_parent:
                self.log('wdir-parent by args')
                wdir_prefix = os.path.join(self.wdir_parent, 'wdir_')    
                self.wdir   = tempfile.mktemp(prefix=wdir_prefix)
            
            
        if os.path.isfile(self.wdir):
            self.error('working-dir exists and its a file:\n%s' %\
                       self.wdir, 12)
        
            
        self.wdir = os.path.abspath(self.wdir)
        self.wdir = os.path.realpath(self.wdir)
        
        ## TODO: cleanup code
        if os.path.isdir(wdir_pre) and wdir_pre != self.wdir:
            if os.path.isdir(self.wdir):
                self.log('working-dir exists, will be emptied')
        
            self.log('move preliminary wdir:\n%s\n%s' % 
                     (wdir_pre, self.wdir),'debug')
            if os.path.isdir(self.wdir):
                if 'dont_clean_working_dir' in args and \
                    args.dont_clean_working_dir:
                    self.log('no wdir-clean-up', 'debug')
                        
                    contents = glob.glob(os.path.join(wdir_pre, '*')) + \
                              glob.glob(os.path.join(wdir_pre, '.*'))
                    for content in contents:
                        content_name = os.path.basename(content)
                        dest         = os.path.join(self.wdir, content_name)
                        
                        remove(dest)
                        shutil.move(content,dest)
                else:
                    remove(self.wdir)
                    shutil.move(wdir_pre, self.wdir)
                                        
            else:
                shutil.move(wdir_pre, self.wdir)
        else:
            if not os.path.isdir(self.wdir):
                os.makedirs(self.wdir)
                #os.chmod(self.wdir_parent, 0777)
        
        
        if self.clear_working_dir:
            if os.path.isdir(self.wdir):
                for path_rel in os.listdir(self.wdir):
                    path = os.path.join(self.wdir, path_rel)
                    shutil.rmtree(path, ignore_errors=True)
            
        
        message = "working-dir: {}".format(self.wdir)
        self.log(message, level='debug')
        
        
        

    def remove_working_dir(self):
        
        if os.path.isdir(self.wdir) and not self.keep_working_dir:
            ## only remove wdir if it is below /tmp
            tmp_dir = os.path.normpath('/tmp')
            if os.path.commonprefix([tmp_dir, self.wdir]) == tmp_dir:
                shutil.rmtree(self.wdir)    
               

 

    def log(self, message, level='debug', speak=True, channel=sys.stdout):       
        self.logger.log(message, level, speak=speak, channel=channel)
            
    def say(self, message):
        print(message)
    
    def error(self, message, exit_code=None):
        self.log(message, level='error')
        if exit_code:
            sys.exit(exit_code)
    

                            
    def show_parse(self, args):

        out = OrderedDict((
            ('parser-arguments', self.cla.arguments),
            ('passed-args', args)
            ))
        
        self.say('## test-parse')
        self.say(to_yaml_safe(out))  
        

## locking, out of service
          
    def lock(self, key, func):
        
        if func in []:
            lock_file_new = os.path.join(self.lockdir, key)
            if os.path.isfile(lock_file_new):
                self.log("another call is running")
                sys.exit(11)
                    
            signal.signal(signal.SIGTERM, self.sig_handler)
            signal.signal(signal.SIGINT, self.sig_handler)
            
            self.lock_file = lock_file_new
            
            if os.path.isfile(self.lock_file):
                sys.exit()
            
            self.unlock_on_exit = True
            self.log("create lock-file: %s" % self.lock_file)
            parent_dir = os.path.dirname(self.lock_file)
            if not os.path.isdir(parent_dir):
                os.makedirs(parent_dir)
            save_file("",self.lock_file)
            

    def sig_handler(self, number=None, frame=None):
        self.unlock()
        sys.exit(12)
        

    def unlock(self, number=None, frame=None):
        if 'unlock_on_exit' in self.__dict__ and self.unlock_on_exit:
            if os.path.isfile(self.lock_file):
                #self.log("remove lock-file %s" % self.lock_file)
                os.remove(self.lock_file)
        
  

    def __del__(self):
        #self.unlock()
        self.remove_working_dir()     



    


