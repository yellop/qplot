from abc import abstractmethod
from cut.fileaccess import afnmatch

class FieldFilter(object):
        
    @abstractmethod
    def apply(self, error_root):
        """error_root is a error-container as returned by ClearTrace.get_error,
           This method drops sub-nodes of error_root.
           """
        pass
    

class FunctionNameFilter(FieldFilter):
    
    def __init__(self,
                 function_names_out=[]):
        self.function_names_out = function_names_out
        
    
    def apply(self, root):
        
        trace = root['trace']
        for name in self.function_names_out:
            for entry in trace:
                if name in entry:
                    entry.pop(name)
        
        return root


    
class NoFuncInfo(FunctionNameFilter):
    
    def __init__(self,
                 function_names_out = ['function', 'class_name', 'arg_values']):
        super(NoFuncInfo, function_names_out)
        



        
class TraceFilter(object):
    pass
    
    @abstractmethod
    def apply(self, error_root):
        """error_root is a error-container as returned by ClearTrace.get_error,
           This method drops some trace-entries from error_root['trace']
           """
        pass
        


class ClassNameFilter(TraceFilter):
    
    def __init__(self,
                 classe_names_out  = None,
                 classe_names_in   = None,
                 
                 ):
        pass
          
class SegmentFilter(TraceFilter):
    """Filter such that one segment of the entry list remains by specifying
    starting and/or ending border. For both borders you may specify the what 
    inside and/or what is outside the segment to remain. So there are 
    up to four specifications: last-outside, first-inside, last-inside, 
    first-outside. 
    
    The method self.match implements whether a specifcation matches an entry
    or not. 
    
    For example if you want to drop all trace-entry before the first one 
    with function-name 'foo', you set first_in = 'foo' and
    let self.match(spec, entry) return entry['function_name'] == self.first_in.   
    """
    
    def __init__(self,
                 ## control omitting classes at beginning
                 ## and at end of trace
                 last_out  = None,
                 first_in  = None,
                 last_in   = None,
                 first_out = None,
                            
                 ):
        self.last_out  = last_out
        self.first_in  = first_in
        self.last_in   = last_in
        self.first_out = first_out
        
    def apply(self, error_root):
        
        trace = error_root['trace']
        
    
        drop_start = self.last_out or self.first_in
        drop_end   = self.last_in  or self.first_out
        
                
        if drop_start:
            ##
            border_present = False
            for entry in trace:
                if self.match(self.last_out, entry) or \
                    self.match(self.first_in, entry):
                    border_present = True
                    break
            
            ##
            if border_present:
                in_segment      = False
                in_segment_rest = False
                trace_start = []
                for entry in trace:    
                    if self.match(self.first_in, entry):
                        in_segment      = True
                        in_segment_rest = True
                        trace_start += [entry]
                    else:
                        if in_segment_rest:
                            trace_start += [entry]
                        else:
                            if in_segment and not self.match(self.last_out, entry):
                                in_segment_rest = True
                                trace_start += [entry]
                    if self.match(self.last_out, entry):
                        in_segment = True
            else:
                trace_start = trace
        else:
            trace_start = trace
            
        
        
        
        if drop_end:
            ##
            border_present = False
            for entry in trace_start:
                if self.match(self.first_out, entry) or \
                   self.match(self.last_in, entry):
                    border_present = True
                    break
                
            ##
            if border_present:
                trace_final     = []
                in_segment      = False
                in_segment_rest = False        
        
                for entry in reversed(trace_start):
                    
                    
                    
                    if self.match(self.last_in, entry):
                        trace_final += [entry]
                        in_segment      = True
                        in_segment_rest = True
                    else:
                        if in_segment_rest:
                            trace_final += [entry]
                        else: 
                            if in_segment and not self.match(self.first_out, entry):
                                trace_final += [entry]
                                in_segment_rest = True
                            
                    if self.match(self.first_out, entry):
                        in_segment = True
                
                trace = list(reversed(trace_final))
                    
            else:
                
                trace = trace_start
        else:
            trace = trace_start
                    
        error_root['trace'] = trace
    
    @abstractmethod    
    def match(self, border_spec, entry):
        """Is entry is a match?"""
        pass
    
    
    

class SegmentFieldFilter(SegmentFilter):
    
    def __init__(self,
                 entry_key = 'class_name',
                 **args
                 ):
        
        super(SegmentFieldFilter, self).__init__(**args)
        self.entry_key = entry_key

    def match(self, border_spec, entry):
        
        if self.entry_key in entry:
            value = entry.get(self.entry_key)
            return self.match_value(border_spec, value)
        else:
            return False

    def match_value(self, border_spec, entry_value):
        return border_spec == entry_value



class SegmentPathFilter(SegmentFieldFilter):
        
    def __init__(self,**args):
        
        args['entry_key'] = 'path'
        super(SegmentPathFilter, self).__init__( **args)
        
    def match_value(self, pattern, path):
        #print(pattern)
        #print(path)
        
        if pattern:
            #print(afnmatch(pattern, path))
            return afnmatch(pattern, path)
        else:
            return None
    
    
 
