"""Functions related to reading and writing files."""

import os, pickle, sys, glob, shutil, re
from cut.mixedtools import shellx
from collections import OrderedDict
import posixpath
import inspect
import importlib


def load_from_shelve(file_name):
    f = open(file_name, 'rb') 
    o = pickle.load(f)
    return o

def save_in_shelve(o, file_name):
    try:
        f = open(file_name, 'wb')
        pickle.dump(o, f) # always create a new database
    except Exception as e:
        print(("save_in_shelve: bugger with %s " % file_name))
        print(e)
        
def save_file(text, path=None, 
              append=False,
              no_change_no_write=False,
              argv_index=1,
              ):
    
    if type(text) == bytes:
        text = text.decode("utf-8")
    
    if path is None:
        path = sys.argv[argv_index]
        
    dont = False
    if no_change_no_write:
        if os.path.isfile(path):
            old = read_file(path)
            if text == old:
                dont = True
                
    if not dont:
        mode = 'a' if append else 'w'
        h_out = open(path,mode)
        h_out.write(text)
        h_out.close()

def write_file(path, text, **args):
    return save_file(text, path, **args)

def read_file(path=None, argv_index=1):
    if path is None:
        path = sys.argv[argv_index]
    h_out = open(path,mode='r')
    out = h_out.read()
    h_out.close()
    return out     

def dir_up(levels, path=None):
    if path is None:    
        parent_frame  = inspect.stack()[1][0]
        path          = parent_frame.f_code.co_filename
        
    start_dir = os.path.join(os.path.dirname(path))
    pd = os.path.pardir
    pds = [ pd ] * levels
    target_dir = os.path.join(start_dir,*pds)
    target_dir_abs = os.path.abspath(target_dir)
    return target_dir_abs

def get_lines(path, comment_suffix=None):
    """Returns list of striped lines from file."""
    fh    = open(path)
    lines = fh.readlines()
    fh.close()
    lines = [ line.strip() for line in lines ]
    if comment_suffix:
        lines = [ line for line in lines 
                  if not line.startswith(comment_suffix)]
    return lines

def get_content_lines(path):
    """Only non-comment lines. Note  that lines are stripped
    before they are checked for comment-suffix."""
    return get_lines(path, comment_suffix="#")


    
def load_classes(module_paths, base_classes=None):
    classes = []
    for module_path in module_paths:
        file_name     = os.path.basename(module_path)
        module_name,_ = os.path.splitext(file_name)
        if module_name == "__init__":
            continue
        
        module  = importlib.import_module(module_name)
              
        for _,c in inspect.getmembers(module):             
            if inspect.isclass(c):
                if not base_classes or not c in base_classes:
                    classes += [c]
        
    return classes




def find(dir_path,  
         filter_func  = lambda path: True, 
         descent_func = lambda dir_path: True ,
         descent_into_hits = True,
         rel_dir_path      = True,
         end_sep           = False,
         ):
    """find abs-paths below dir_path an which filter_func return true.
    It descends only into subdirs on the abs-path of which 
    descent_func yield true.
    """ 

    out = []
    for root, dirs, files in os.walk(dir_path):

        dirs_indices_out = []
        
        for path_rel in files:
            path = os.path.join(root, path_rel)
            if filter_func(path):
                out.append(path)

        for i, path_rel in enumerate(dirs):
            path = os.path.join(root, path_rel)
            
            if not descent_func(path):
                dirs_indices_out.append(i)
                
            if filter_func(path):
                out.append(path)
                if not descent_into_hits:
                    dirs_indices_out.append(i)
                    
        for i in reversed(dirs_indices_out):
            dirs.pop(i) 
                
    if rel_dir_path:
        out = [ os.path.relpath(path, dir_path) for path in out ]
        

    if end_sep:
        out_ = []
        for path in out:
            if rel_dir_path:
                path_ = os.path.join(dir_path, path)
            else:
                path_ = path
            if os.path.isdir(path_):
                out_.append(path + os.path.sep)
            else:
                out_.append(path)
        out = out_
        
    
    
    return out



def afnmatch(pattern, path):
    """fnmatch that undestands '**'"""
    ## TODO: escaped sep
    pattern_ = re.escape(pattern)
    pattern_ = re.sub(r'\\\*\\\*', '.*', pattern_)
    pattern_ = re.sub(r'\\\*',  '[^%s]*' % os.path.sep, pattern_)
    return re.match(pattern_, path)
    


def create_dir(dir_path, wipe=False):
    """Create directories in dir_path, likewise mkdir -P."""
    
    if wipe:
        if os.path.exists(dir_path):
            shutil.rmtree(dir_path)
    os.makedirs(dir_path)
    

def empty_dir(path):
    """Create directory ala rm -rf path; mkdir -p path"""
    create_dir(path, wipe=True) 
    

def strip_path(path):
    
    path    = posixpath.normpath(path)
    pattern_curd = '({}{})+'.format(os.path.curdir, os.path.sep)
    pattern_seps = '({})+'.format(os.path.sep)
    pattern_curo = '({})'.format(os.path.curdir)
    
    if re.match('^' + pattern_curd, path):
        path = re.sub('^' + pattern_curd, '', path)  
    
    if re.match('(.*?)' + pattern_seps + '$', path):
        path = re.sub('(.*?)' + pattern_seps + '$', '\\1', path)
    
    if re.match('^' + pattern_curo + '$', path):
        path = ''
    
    return path

def abspath(*path_parts):
    """make a nromalized abs-path from path-parts"""
    path        = os.path.join(*path_parts)
    path_abs    = os.path.abspath(path)
    path_norm   = os.path.normpath(path_abs)
    return path_norm
    
def abspath_posix(*path_parts):
    """like abspath but force posix-mode"""
    path        = posixpath.join(*path_parts)
    path_abs    = posixpath.abspath(path)
    path_norm   = posixpath.normpath(path_abs)
    return path_norm



def posix_path(path, os_path=os.path):
    """
    @param path: a string that is path descriptor that 
        can be processed by the os_path module. If os_path=os.path,
        the descriptor has to fit to the system on which the function 
        is called.
    @param os_path: the module that is used to interpret path
    @return: the path described by path in posix-notation
    """
            
    elements = split_path(path, os_path=os_path)
    ## shortcut
    if os_path.isabs(path) and not elements[0] is '':
        elements = [''] + elements
    
    return '/'.join(elements)
    
    
    
def split_path(path, os_path=os.path):
    """
    @param path: path descriptor as understood by the module os_path
    @param os_path: the path-processing module used to interpret path 
    @return: list of components of the path. If path is absolute, the 
        first element is ''. If path is a dir-path (ends with a /),
        the last element is ''.
    """
    prefix, suffix = os_path.split(path)
    
    if prefix == path:
        return [ '' ]
    if prefix:
        splited_prefix = split_path(prefix, os_path=os_path)
        out = splited_prefix + [ suffix ]
    else:
        out = [ suffix ]
    return out



def system_path(*path_descriptors):
    """
    @param path_descriptors: a path_descriptor is path part in posix-notation.
    @return: a string that describes the corresponding path as demanded by the os.
    
    Conterpart of posix_path, but it can take two or more path descriptors 
    that are joint to one path.
       """
               
    if len(path_descriptors) > 1:
        
        head = system_path(path_descriptors[0])
        rargs = path_descriptors[1:]
        rest = system_path(*rargs)

        return os.path.join(head, rest)
    
    else:
        
        path_descriptor = path_descriptors[0]
            
        parts = path_descriptor.split('/')
        
        new_parts = []
        
        part = parts[0]
        if part == '':
            parts.pop(0)
            new_parts += [ os.path.sep ]
        
        for part in parts:
            if part == '..':
                new_parts += [ os.path.pardir ]
            else:
                new_parts += [ part ]
         
        return os.path.join(*new_parts)


def get_all_files(path):
    file_names = []
    for listing in os.walk(path):
        file_names_in_dir = len(listing[2])
        if file_names_in_dir > 0: ## leaf level reached
            for base_name in listing[2]:
                file_name  = listing[0] + '/' + base_name
                file_names.append(file_name)
    return file_names


def get_all_dirs(path):
    dir_names = []
    for listing in os.walk(path):
        dir_names_in_dir = len(listing[1])
        if dir_names_in_dir > 0: ## leaf level reached
            for base_name in listing[1]:
                dir_name  = listing[0] + '/' + base_name
                dir_names.append(dir_name)
    return dir_names


def switch_suffix(file_name, new_suffix):
    return ".".join( file_name.split('.')[:-1] + [new_suffix])
    
def get_files_by_suffix(d, suffices):
    """Returns list of absolute pathes of the files in d with given suffix.
    If suffix is a list either suffix in the list is a match."""
    if not type(suffices) == list:
        suffices = [suffices]   
    dirs = get_all_dirs(d)
    out = []
    for d in dirs:
        for suffix in suffices:
            out += glob.glob(os.path.join(d, '*%s' % suffix))
    
    return out
    

def get_all_suffices(d):
    
    base_names = os.listdir(d)
    parts      = [ os.path.splitext(p) for p in base_names]  
    suffices   = [ suffix[1:] for (rest,suffix) in parts if rest and suffix]

    return sorted(list(set(suffices)))


def basename_minus_1(path):
    return os.path.splitext(os.path.basename(path))[0]


def homedir():
    return os.path.expanduser('~') + os.path.sep



def copy_content(source, dest_base, dest_dir=None, source_base='.', 
                 mode=None,
                 ignore_same=True):    
    """Copy single file or all _files_ from a dir
    keeping the files permissions unless mode is given."""    
    
    source_abs = os.path.join(source_base, source)
    
    if os.path.isdir(source_abs):
        source_dir = source_abs
        source_files = [ x for x in glob.glob(os.path.join(source_abs, '*')) 
                            if os.path.isfile(x) ]
    elif os.path.isfile(source_abs):
        source_dir = os.path.dirname(source_abs)
        source_files = [ source_abs ]    
    else:
        raise Exception()
    
    if not dest_dir:
        dest_dir = os.path.join(dest_base, source_dir)
    
    dest_dir = os.path.normpath(dest_dir)

    if not os.path.isdir(dest_dir):
        create_dir(dest_dir)
    
    print(("copy files to dest: %s" % dest_dir))    
    
    
    for source_file in source_files:
        # # keep permission as are
        file_name = os.path.basename(source_file)
        dest_path = os.path.join(dest_dir, file_name)

        shutil.copy2(source_file, dest_dir)
        if mode:
            ## recursive os.chmod? 
                
            shellx('chmod -R %s %s' % (mode, dest_path))
        

def copy_dir(source_dir, dest_base, dest_dir=None, source_base='.', mode=None):    
    
    source_dir = os.path.join(source_base, source_dir)
    
    if not dest_dir:
        dest_dir = os.path.join(dest_base, source_dir)
        
    
    dest_dir = os.path.normpath(dest_dir)
    
    #print(glob.glob("*"))
    
    if os.path.isdir(dest_dir):
        shutil.rmtree(dest_dir) 
        #create_dir(dest_dir)

    print(("copy files to dest: %s" % dest_dir))    
        
    shutil.copytree(source_dir, dest_dir)
    if mode:
    ## recursive os.chmod? 
        shellx('chmod -R %s %s' % (mode, dest_dir))


def replace_in_file(path, replacements):
    """replace by regexps, replacements maps re-pattern to re-replacement-string"""

    replacements = OrderedDict(replacements)

    content  = read_file(path)

    matches_d = OrderedDict()
    
    content_ = content
    
    for pattern, replacement in replacements.items():
        
        matches =\
        matches_d[pattern] =  re.search(pattern, content, flags=re.MULTILINE)
        
        if matches: 
            content_ = re.sub(pattern, replacement, content_, flags = re.MULTILINE)
        
    
    save_file(content_, path, no_change_no_write=True)
    
    return matches_d


def remove(path):
    """remove path, no questions"""  
    if os.path.isdir(path):
        shutil.rmtree(path, True)
    if os.path.exists(path):
        os.remove(path)
    


    
if __name__ == "__main__":
    pass

    
    
    
    
    