# -*- coding: utf-8 -*-
"""
The cut (common utils) packet provide auxiliary code that may be helpful for arbitrary project.
The main motivation is to keep technical details out the way when coding 
the real thing.  

Copyright (C) 2010-2018 Lorenz Weizsäcker
The code of this package is released under 
the Apache License, Version 2.0. a copy of which can be obtained 
at http://www.apache.org/licenses/LICENSE-2.0 .
"""
