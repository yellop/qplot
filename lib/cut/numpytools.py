"""Some often used shortcuts for using numpy."""

from numpy import array, concatenate ,transpose, shape, reshape,\
                  empty, zeros, size, repeat,\
                  prod, sqrt, inf, nanargmax,\
                  ones,  isnan, var, nansum, nan, sum, mean,\
                  iscomplexobj, isrealobj, linalg, ceil, median

from numpy.linalg import norm
from .container import  indices, peel, multi_range
from copy import deepcopy
from numpy.matlib import rand


def quantile(X,q):
    """Returns min z s.t. z\in X, #{x\in X| x<=z } >= q |X|.""" 
    assert len(shape(X)) == 1
    assert 0 <= q and q <= 1
    
    n  = len(X)*1.
    X_ = sorted(X) 
    m  = int(ceil(q*n) - 1)
    z  = X_[m]
    
    for z_ in X:
        if sum(X <= z_)/n >= q:
            assert z_ >= z
    
    return z

def quantiles(X, k):
    return [ quantile(X,1.*i/k) for i in range(1,k+1) ]


def numberize(s):
    """tries to convert string s into a number"""
    try:
        v = int(s)    
    except:
        try:
            v = float(s)
        except:
            v = None
    
    return v


def number_like(x):
    
    try:
        return float(x) == x
    except:
        if type(x) == type(''):
            return x.isnumeric()

    return False



def potential_nummeric_array( an_object ):
    """wether the object can be turned into an nummeric array or not"""
    def is_nummeric_simple( x):
        return isrealobj(x) or iscomplexobj(x)  
      
    try:
    
        out = is_nummeric_simple(array(an_object))
    except:
        out = False
    return out

def is_real_number(x):
    try:
        return float(x) == x
    except:
        return False

def is_int(x):
    try:
        return int(x) == x
    except:
        return False

def round_to_int(x):
    return int(round(x))

def vlen(w):
    w = array(w)
    w = w.ravel()
    return sqrt(sum(w**2))



def fmatrix(f, xxx_todo_changeme):
    (n,m) = xxx_todo_changeme
    out = empty((n,m))
    for i in range(n):
        for j in range(m):
            out[i,j] = f(i,j)
    return out
                                                   
def tp(v):
    return transpose(v)

def shapes(*a):
    return [ shape(x) for x in a ]

def same_shape(A, value=0):
    return value * ones(shape(A))

def trim(vector,l):
    n = len(vector)
    if n >= l:
        return vector[:l]    
    else:
        return concatenate( (vector, nan*empty((l-n)) ), dtype=vector.dtype)

def select_rows(A, selector):
    n  = sum(selector != 0)
    d  = shape(A)[1]
    #print shapes(A, selector)
    A_ = A[ repeat(col(selector),d, axis=1) ]
    return reshape(A_, (n,d))

def unit_vector(n,j):
    out = zeros(n)
    out[j] = 1
    return out
    
    

def marginal_array(n,m):
    return reshape(array([]), (n,m))

def column_vector(v):
    #print shape(v)
    v = array([v])
    
    assert size(v) == 0 or max(shape(v) ) == prod(shape(v)) 
    n = len(array(v).ravel())
    
    #print "VVV", n, v
    return reshape(v, (n,1))

def row(v):
    return tp(column_vector(v))

def col(v):
    return column_vector(v)

def repeat_row(row,n):
    m = size(row)
    return repeat(reshape(row,(1,m)), n, axis=0)

def cut_column(A,j):
    n = shape(A)[0]
    return reshape( A[:,j], ( n , 1 )  )

def append_value(X,c=1.):
    
    assert len(shape(X)) == 2

    return concatenate( ( X, c * ones( (len(X),1) )  ), axis=1)

def a2sign(a):
    return ((a > 0 ) - 0.5)*2

def bool2sign(b):
    return 1 if b else -1 # = a2sign(x)


def count_sign(y,s):
    return sum( s * y > 0 )

def lunion(A,B):
    return sorted(list(set(A).union(set(B)))) 

def lcut(A,B):
    return sorted(list(set(A).intersection(set(B))))

def ldiff(A,B):
    return [ x for x in A if not x in B ]


def indices_equal(A,v):
    return array(indices(A.ravel()))[ (A == v).ravel() ]


def invert_norm(x):
    return x == 0 and 1 or 1/x
    
def normalize_vector(v):
    return v * invert_norm(norm(v))


def normalize_matrix_columns(M):
    
    M_0 = zeros(shape(M))
    for j in range(shape(M)[1]):    
        M_0[:,j]  = normalize_vector(M[:,j])
    
    return M_0    

def normalize_matrix_rows(M):
    return tp(normalize_matrix_columns(tp(M)))


def normalize(liste):
    v = liste
    variance = var(v)
    if variance == 0:
        return [0] * len(v)
    else: 
        return list( (v - mean(v)) / sqrt(variance) ) 


def sparse_rows(matrix):
    out = []
    for row in matrix:
        row_sparse = {}
        for j in range(len(row)):
            entry = row[j]
            if entry:
                row_sparse[j] = entry
        out.append(row_sparse)
    return out
    
def unsparse_rows(matrix, indices=None ):
    if not indices:
        indices = set()
        for row in matrix:
            indices = indices.union(list(row.keys())) 
    max_index = max(indices)
    out = marginal_array(0,max_index + 1)
    for row in matrix:
        new_row = reshape(zeros(max_index + 1), (1, max_index +1))
        for key in list(row.keys()):
            new_row[0,key] = row[key]
        out = concatenate((out,new_row), axis=0)
    return out

def test_matrix_equality(A,B):
    for i in range(len(A)):
        for j in range(len(A[i])):
            assert A[i][j] == B[i][j]


def partition_index(element, partition):
    """Finds the index of the part in partition that 
    contains element. If element is contained in more than 
    one part, the index of the first part is returned. If 
    element is contained in no part, then the return value is None."""
    for i in indices(partition):
        if element in partition[i]:
            return i
    return None
    
def subarray(A, mis):
    
    assert len(A.shape) == len(mis)
    assert all([ A.shape[i] >= max( mis[i] + [0] ) for i in indices(mis)])

    for i,mi in enumerate(mis):
        S = [ slice(s) for s in A.shape ]
        S[i] = mi
        A = A[S]
        

    assert A.size == prod([len(mi) for mi in mis ])
    return A
        

def anti_ravel(dims, aligned_index):
    """counterpart of ravel: transform aligned index to multi
    index according to dims"""
    
    def get_it(dims, aligned_index):
        new_dims = dims[1:]  
        p = prod(new_dims)
        i = int(aligned_index // p)
        if new_dims: 
            out = [i]
            out.extend(get_it(new_dims, aligned_index - i * p))
            return out
        else: 
            return [i]

    return tuple(get_it(dims, aligned_index))



def argmax_nd(A):
    return anti_ravel(shape(A), nanargmax(A.ravel()) )

#
#def k_nd_argmax(A,k, unique=False, only='indices'):
#    """Returns the top k maximizers of A. If unique==True, all indices 
#    that yield the i-th greatest value (i=1..k) are returned as a list.
#    
#    only specifies the output: indices, values, both
#    """
#    
#    values  = []
#    indices = []
#    
#    if not unique:
#        
#        for i in range(k):
#            index    = nd_argmax(A)
#            indices.append(index)
#            values.append(A[index])
#            A[index] = -inf
#        for i in range(k):
#            A[indices[i]] = values[i]
#    else:
#        
#        index     = nd_argmax(A)
#        value     = A[index]
#        new_value = value
#            
#            
#        for i in range(k):
#            indices.append([])
#            values.append(value)
#            
#            while new_value == value:
#                A[index] = -inf
#                indices[i].append(index)
#
#                index     = nd_argmax(A)
#                new_value = A[index]
#                
#            value = new_value
#            
#        for i in range(k):
#            for index in indices[i]:
#                A[index] = values[i]  
#            
#    if   only=='indices':
#        return indices
#    elif only=='values':
#        return values
#    else:
#        return indices, values




def get_maximizer( Z, value_index=-1, without_value=True, unpacked=True ):
  
    max_value  = -inf
    for z in Z:
        value = z[value_index]
        if value > max_value:
            max_value = value
            out       = z
    if without_value:
  
        assert type(out) in [type(tuple()),type(list())]
        if type(out) is type(tuple()):
            out = list(out)
            out.pop(value_index)
            out = tuple(out)
        else:
            out.pop(value_index)
    if unpacked:
        if len(out) == 1:
            out = out[0]
    
    return out


def get_minimizer( Z, value_index=-1, without_value=True, unpacked=True ):

    Z_ = []
    for z in Z:
        z_ = list(deepcopy(z))
        z_[value_index] = - z_[value_index]   
        Z_ += [ z_ ]
    return get_maximizer(Z_, value_index, without_value, unpacked)

        
def first(A, axis=0):
    """Selects first entry in axis."""

    S  = shape(A)
    if not S[axis]:
        return A
    else:
        selection = [ None ] * len(S)
        for i,s in enumerate(S):
            selection[i] = slice(1) if i == axis else slice(s)  
            
        return A[selection]

    

def nan_mean(v, axis=0):
    """Computes the mean dropping all nans."""
    
    n_all      = shape(array(v))[axis]
    n_nans     = sum(isnan(v),axis=axis) 
    n_non_nans = array(n_all - n_nans,dtype=float)
    sums       = nansum(v, axis=axis)
    return  sums / n_non_nans

    
def nd_nan_mean(v):
    out = v
    for i in range(len(shape(v)) - 1, -1, -1):
        out = nan_mean(out, axis=i)
    return out 
    

def nan_std(v, axis=0):
    """Computes the stadart deviation dropping all nans."""
    
    return sqrt(nan_mean(v**2, axis=axis) - nan_mean(v,axis=axis)**2)
   

def nan_median(v):
    v = array(v)
    assert size(v) == max(v.shape)
    v = v.ravel()
    v_ = v[ - isnan(v) ]
    median = quantile(v_,1./2)
    return median
    
    

def reduce_value(value):
    """If value is some container with a single entry, 
    we peel the entry out and take it as value. 
    The motivation is that when we aggregate numpy arrays
    superfluece dimensions often survive. 
    
    If value is a list or tuple it calls itself on the elements.""" 
    ## values that are lists or tuples undergo a
    ## peeling treatment
    ## That means: list/tuple/arrays as cube value is not possible.
    ## If you pass such, and len <= 1, container is peeled away,
    ## and if len > 1, new dimensions will be derived  

    try:
        len(value)
    except:
        return value 
    else:
        if   len(value) == 0:
            return value
        elif len(value) == 1:
            
            if type(value) in [list, tuple]:
                return peel(value)
            
            if type(value) == type(array(None)):
                value_ = value.ravel()
                if len(value_) == 1:
                    return value_[0]
        else:
            if type(value) in [list, tuple]:
                return array([ reduce_value(v) for v in value ])
            else:
                return value


def check_m_index(m_index, A):
    """Whether m_index has as many elements as A has dims and
       the elements are not out range"""
       
    assert len(m_index) == len(shape(A)) 
    assert all( [ type(i) is int for i in m_index ])
    assert all( [ m_index[i]< A.shape[i] for i in indices(m_index) ])
     
def m_indices(A,names=None):
    
    return multi_range(A.shape,names)
    

### peewit unused

def parametrized_line(w,b):
    """turn normal vector - offset representation 
    of a line into parametrized reprsentation""" 
    assert size(w) == 2
    
    w1   = w[0]
    w2   = w[1]
    nw   = w1**2 + w2**2 
    v_0  = b * 1./nw  * w 
    
    if w1:
        v_1 = ( - w2/w1 , 1)
    else:
        v_1 = ( 1 , - w1/w2)
    
    v_1 = v_1 / linalg.norm(v_1) #sqrt( v_1[0]**2. + v_1[1]**2.  )
    
    return v_0,v_1




if __name__ == '__main__':
    
    
    if 0:
        a = [ 4, nan, 6, 0, 99,100, nan, nan, nan, nan]
        b = [ 3,55,22,33,666,55,1,2,3,4,544]
        print(nan_median(a), median(a))
        print(quantile(b,0.9))
    if 1:
        f = rand(4,5)
        
        print(f)
        print(first(f, axis=0))

    
    