"""The module define some general purpose base classes."""

import sys
from copy import deepcopy
from cut.mixedtools import stems_from_2, to_yaml_safe, get_args



class Base(object):
    """The very ancestor for most class we use. Its main feature is a more 
    comfortable initialization through the init method. The ancestor init-
    methods are called automatically and the init arguments are 
    automitically bound as attributes. See __init__ for details.
    """

    def __init__(self, *positional_args, **named_args):
        """Calls the init function of any ancestors class (starting from top) and
        of the proper class, provided the respective init is defined. 
        
        If more than one of the init-method takes positional args, 
        the positional args for all these inits must be identical in 
        names, number, and order. In contrast to default arg-passing scheme, 
        extra positional args  are not taken as values for follow-up optional 
        args. E.g. to pass a value via an optional arg you have to pass it 
        by name (..,x=2,..).
        
        The mandatory and optional arguments of the called init-functions become 
        attributes. Since descendant init are called later they overwrite previous 
        values if the names are equal.
         
        The default value for an optional argument is deepcopied such that each 
        instance has its proper value even if the default value applies. 
        As deepcopying does not work for any type, you have take care that 
        default values can be deepcopied.
        
        
        TODO: sometimes it is helpful to know whether an attribute stemming 
        from an init-arg has passed at init-call or not.
        Though we can always test whether is differs from default or not, this 
        test is demand to know the current default. Also, it does not exactly answer 
        the question. For instance we might set a default value mainly as example 
        value. Or to make the init run in cases where we only want to create an dummy 
        object for accessing methods that we do not want make static at some point. 
        - self.passed_optionals
        - method to unset not passed values? 
        
        """
        
        
        ancestors = Base.get_base_ancestors(self.__class__) + [ self.__class__ ]

        ## get all default-dicts and merge them
        merged_default_dict = {}
        for ancestor in ancestors:
              
            optionals_dict = get_args(ancestor.init)[2]
            merged_default_dict.update(optionals_dict)
        
        ## deepcopy defaults   
        for key in merged_default_dict:
            merged_default_dict[key] = deepcopy(merged_default_dict[key])
        
        ## check wether named_args are known by at least one init to be called
        for key in named_args:
            if not key in merged_default_dict:
                print(ancestors)
                print(merged_default_dict)
                error_text = 'Unknown optional argument for %s: %s' %\
                    (self.__class__, key)
                raise Exception(error_text) 
        
        ## check uniqueness of mandatory args  
        mandatory_names = None
        for ancestor in ancestors:
            if  Base.defines_proper_init(ancestor):

                mandatory_names_ = get_args(ancestor.init)[0]
                if mandatory_names_:
                    if mandatory_names == None:
                        mandatory_names = mandatory_names_
                        first           = ancestor
                        assert len(positional_args) == len(mandatory_names),\
                            ("%s.init expects %d postional args"+\
                            "but %d are given")%\
                            (first.__name__,len(mandatory_names),len(positional_args))
                    else:
                        assert mandatory_names_ == mandatory_names,\
                            ("init-methods in descendant-lines from Base must have\n"+\
                            "same mandatory args (names + order) which is not the case:\n"+\
                            "%s: %s \n%s: %s ")%\
                            (first.__name__,    mandatory_names, 
                             ancestor.__name__, mandatory_names_)
                    
             
                 
        ### call inits   
        
        
        for ancestor in ancestors:
            
            if  self.init_condition(ancestor, positional_args, named_args):
                #Base.defines_proper_init(ancestor):
                
                ## set mandatories as members
                mandatory_names = get_args(ancestor.init)[0]         
                mandatory_dict  = {}
                for i in range(len(mandatory_names)):
                    mandatory_dict[mandatory_names[i]] = positional_args[i]
                self.set_attributes(mandatory_dict)
                
                ## prepare optional args and set them as members
                named_args_     = {}
                optional_names  = get_args(ancestor.init)[1]  
                for key in optional_names:
                    if key in self.__dict__:
                        named_args_[key] = self.__dict__[key]
                    else:
                        if key in named_args:
                            named_args_[key] = named_args[key]
                        else:
                            named_args_[key] = merged_default_dict[key]
                self.set_attributes(named_args_)

                ## call init             
                if len(mandatory_names):
                    positional_args_ = positional_args
                else:
                    positional_args_ = []
                
                ancestor.init(self, *positional_args_, **named_args_)
                
                

    def set_attributes(self, provided_dict):
        for (member_name, value) in list(provided_dict.items()):
            self.__setattr__(member_name, value)
    

    @staticmethod          
    def get_base_ancestors(cl):
        """Return list of ancestors, closest last."""
        mro = list(cl.__mro__)
        mro.reverse()     
        ## neither object nor Base itself, nor proper class are counted as ancestor
        ancestors = mro[2:-1]
        ## filter Base-descendants
        ancestors = [ a for a in ancestors if stems_from_2(a,Base)]
        return ancestors
    
    @staticmethod
    def defines_proper_init(cl):
        
        ancestor_inits = [ a.init for a in Base.get_base_ancestors(cl)
                           if hasattr(a, 'init') ]
        return hasattr(cl, 'init') and not cl.init in ancestor_inits 
    
    def init_condition(self, ancestor, positional_args, named_args):
        
        if not Base.defines_proper_init(ancestor):
            return False
    
        mandatory_names = get_args(ancestor.init)[0]  
        
        n_positional = len(positional_args)     
        n_mandatory  = len(mandatory_names)
            
        return n_mandatory == 0 or n_mandatory == n_positional
        
    
    def proper_members(self):
        
        dummy = Named()
        
        members_dummy = list(dummy.__dict__.keys())
        members       = list(self.__dict__.keys())
        
        extra = list(set(members).difference(set(members_dummy)))
        
        extra_d  = dict([ (k,self.__dict__[k]) for k in extra ])
        
        
        return extra_d
    
    def added_members(self):
        
        dummy = self.__class__()
        
        members_dummy = list(dummy.__dict__.keys())
        members       = list(self.__dict__.keys())
        
        extra = list(set(members).difference(set(members_dummy)))
        
    
        return dict([ (k,self.__dict__[k]) for k in extra ])
  
    def to_yaml(self):
        return to_yaml_safe(self)

    def init(self):
        pass
    
    
     


    



    

class Named(Base):
    """Common base class that provides some more or less helpfull
    general purpose methods. If there is no reason against, we make 
    objects always of from this type.
    """
     
    def init(self, 
              name              = None,
              verbosity         = 1,
             ):    
        """
        @param name: string to name the object
        @param verbosity: the verbosity level of the object, 0 meaning dump
            If a message is to print, the level of the message is compared to 
            verbosity of the object and only passed if the latter is not lower
            the message level. 
            
        @todo: the message/verbosity handling should be redone applying 
            the python logging framework 
        """ 
        
        self.last_tick = {}

    def classname(self):
        return self.__class__.__name__ 
        
    def get_name(self):
        return self.classname()
  

    def send(self,  *messages, **parameters):
        """
        Passes logging messages to stdout.
        
        Since we a *-argument we cannot name the parameters in the signature.
        The parameters are:
        level: from which verbosity of self should the message be passed
        linebreaks, lb: number of linebreaks to append to the message
        """
        
        ## digest parameters    
        if 'level' in parameters:
            message_level = parameters['level']
        else:
            message_level = 1
            
        if   'linebreaks' in parameters:
            n_linebreaks = parameters['linebreaks']
        elif 'lb' in parameters:
            n_linebreaks = parameters['lb']
        else: 
            n_linebreaks = 1


        ## go
        if self.verbosity >= message_level:
        
            #for part in messages:
            #    print part, end=' ')
            
            
            print(' '.join(messages))
            
            if n_linebreaks > 0: 
                print(('\n' * (n_linebreaks-1)))
                        
            sys.stdout.flush()
        
    
    def bugger(self,*messages):
        if len(messages) == 0:
            message = 'Bugger.'
        else:
            message = 'Bugger: ' + ' '.join([ str(m) for m in messages])
        raise Exception(message) 
    
    def has(self, attribute_name):
        try:
            self.__getattribute__(str(attribute_name))
        except AttributeError:
            return False
        else:
            return True
        
    def flag(self, attribute_name):
        return self.has(attribute_name) and self.__getattribute__(str(attribute_name))


 
class Object(object):
    """Can serve as container with .-access to compounds."""
    pass
 

if __name__ == '__main__':
    
    
    A = Named()
    
    b = [ 43 , A , 'sdfsd']
    A.f =5
    
    print((A.reduced()))
    
