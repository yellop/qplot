import sys, re, time, datetime, yaml, inspect, os, multiprocessing, glob, imp
from subprocess import Popen, PIPE
from collections import OrderedDict, Container
from copy import deepcopy
from fnmatch import fnmatch
from yaml.representer import BaseRepresenter
from yaml.constructor import MappingNode, ConstructorError
#from yaml.emitter import ScalarAnalysis
#from yaml.dumper import SafeDumper, Dumper
import shlex


def frugal_permutation( n, seed=1):
    """Compute pseudo-pseudo-random permutation using linear 
    congruence generator on multiple rounds. 
    
    @param n: length of computed permutation

    This random permutation generator is meant to be used 
    for instance to select sub-sample from data sets. In such cases 
    we only need 'weak randomness' but want to be able to 
    easily reproduce the output permutation for a given seed. 
    
    For information on LCG see 
    http://random.mat.sbg.ac.at/results/karl/server/node4.html 
    as of 07/26/2012. 
    """    
    if seed == None:
        seed = 1
        
    r = 64        ##  number of mixing rounds

    m = 2**64     ## base
    a = 19030423  ## factor, Kolmogorovs date of birth, coprime to base

    
    assert 0 < n and n < m
    assert r > 1, """One round is to weak, it likely to produce 
                    'regular' sequences over incremented seeds."""
    assert type(seed) in [int,int], type(seed)
    assert not seed % m == 0, "This would yield range(n) as output."
    
    
    ## compute r*n 'random' numbers in {0,..,2**64-1} 
    ## chucked in r rows having n entries 
    
    random_rows  = [ None ] * r
    x            = seed
    
    for k in range(r):
        row = [ None ] * n
        for i in range(n): 
            row[i] = x = ( a * x ) % m
        random_rows[k] = row
         
        
    ## reduce each random row to a permutation over {0,..,n-1} 
    ## and apply the permutations iteratively      
    
    output_permutation = list(range(n)) ## initial permutation
    
    for k in range(r):
        permutation         = [ None ] * n
        remaining_elements  = list(range(n))

        for i in range(n):    
            n_ = len(remaining_elements)
            index_next     = random_rows[k][i] % n_        
            permutation[i] = remaining_elements.pop(index_next)
        
        
        output_permutation = [output_permutation[i] for i in permutation ]
        
    return output_permutation






def shell(command_fields,  **opts):
    return shello(command_fields, **opts)

def shello(command_fields, **opts):
    return shelloex(command_fields, **opts)['stdout']

def shelle(command_fields, **opts):
    return shelloex(command_fields, **opts)['stderr']

def shellx(command_fields, **opts):
    return shelloex(command_fields, **opts)['exitst']



## https://pymotw.com/3/subprocess/


def shelloex(command_fields, 
             show       = '', 
             env        = None,
             env_extra  = {},
             output     = 'dict',
             bg         = False,
             bg_wait    = 1,
             dry_run    = False,
             print_func = None,
             cwd        = None,
             raise_cmd_not_found = True, 
             raise_exit_code     = False, 
             ):
    """call a single command command_fields[0] with args
    command_fields[1:]. Cares for quotation, and escapes.
    """
    
    ## 
    def printit(x):
        if print_func:
            print_func(x)
        else:
            print(x)
    
    
    ## setup env 
    if env or env_extra:
        if not env:
            env = os.environ
        for k in env_extra:
            env[k] = str(env_extra[k])
    
    ## setup command_fields
    if type(command_fields) in [ str, type('')]:
        command_string = command_fields.strip()
        command_fields = shlex.split(command_string)
    else:
        command_fields = [ str(e) for e in command_fields]
        command_string = " ".join(command_fields)
    
    ## TODO: should we drop this?
    #command_fields = [ x.strip() for x in command_fields ] 
    
    
    
    ## init output
    oes = OrderedDict()
    oes['cmdstr'] = " ".join(command_fields) 
    oes['currwd'] = cwd if cwd else os.getcwd()
    oes['procid'] = None 
    oes['stdout'] = None
    oes['stderr'] = None
    oes['exitst'] = None
    oes['popeno'] = None 
    
    ##
    if 'v' in show:
        if not env:
            env = os.environ
        env_string = '\n'.join('%s: %s' % (str(k),str(v)) for k,v in list(env.items()) )
        printit('envvar:\n'  + env_string)
    
    if 'w' in show and cwd:
        printit('currwd: '  + cwd)
    
    if 'c' in show:
        printit('cmdstr: '  + " ".join(command_fields))
    
    ## go
    if dry_run:
        printit('dryrun: yes')
    else:
                    
        command_parts = []
        part = []
        for word in command_fields:
            if word != '|':
                part += [ word ]
            else:
                assert part
                command_parts += [ part ]
                part = []
        command_parts += [ part ]
        
        assert command_parts
                    
        stdin  = None
        ps     = []
        for part in command_parts:
            p = None
            try:
                p      = Popen(part, 
                               stdin=stdin, stdout=PIPE, stderr=PIPE, 
                               cwd=cwd)
            except OSError as e:
                if raise_cmd_not_found:
                    raise e
                else:
                    printit("\ncaught OSError on cmd '%s': %s" % 
                            (' '.join(part), str(e)))
                    
                    if str(e).count('[Errno 2]'):
                        oes['exitst'] = 127
                    
                    break
            stdin  = p.stdout
            pid    = p.pid
            ps    += [p]
        
        if p:
            if bg:
                time.sleep(bg_wait)
                stdout = bytes("", encoding='utf-8')
                stderr = bytes("", encoding='utf-8')
                
                exitst = p.poll()
                oes['popeno'] = p 

                
                
            else:
                stdout, stderr = p.communicate()                        
                p.wait()
                exitst         = p.returncode
        
        
            ## digest outcomes
            oes['procid'] = int(pid) 
            oes['stdout'] = stdout.decode("utf-8", errors='replace') 
            oes['stderr'] = stderr.decode("utf-8", errors='replace')
            oes['exitst'] = exitst
          

        
        ## no, format depends on k too
        #show_map = OrderedDict((
        #    'c', 'cmdstr',
        #    'o', 'stdout',
        #    'e', 'strerr',
        #    'x', 'exitst',
        #    'p', 'procid',
        #    ))
        
        ## speak
        show_lower = str(show).lower()
        
        
        def strstrip(s):
            if s:
                return str(s).strip()
            else:
                return ''
        
        if 'o' in show_lower:
            stdout_string = strstrip(oes['stdout']) 
            if 'O' in show or stdout_string:
                printit('stdout:\n' + stdout_string)
        if 'e' in show_lower:
            stderr_string = strstrip(oes['stderr'])
            if 'E' in show or stderr_string:
                printit('stderr:\n' + stderr_string)
        if 'x' in show_lower:
            exit_code = oes['exitst']
            if 'X' in show or exit_code: 
                printit('exitst: '  + str(exit_code))
        if 'p' in show_lower:
            pid = oes['procid']
            if 'P' in show or pid is not None: 
                printit('procid: '  + str(pid))
    
        
        ##
        if raise_exit_code and oes['exitst']:
            raise Exception("non-zero exit-code\n" + to_yaml(oes))
        
    ## 
    if   output == 'list':
        return list(oes.values())
    elif output == 'dict':
        return oes



    
def timeout_call(method, obj, timeout=600, default_value=None, silent=False):
        
    def g(q,x):
        y = method(x)
        q.put(x)
        q.put(y)
    
    q       = multiprocessing.Queue()
    process = multiprocessing.Process(target=g, args=(q,obj))        
    process.start()
    process.join(timeout=timeout)
    
    if not q.empty():
        obj_ = q.get()
        try:
            for member in obj_.__dict__:
                obj.__dict__[member] = obj_.__dict__[member]
        except AttributeError:
            pass
        y = q.get()  
    else:
        if not silent:
            print('run out of time')
        y    = default_value
        
    process.terminate()

    return y


def tik(key=None, pause=True):
    """
    start stop watch
    """
    tok(key=key, pause=pause, tik=True)


def tok(key=None, stage=None, mcs=False, pause=True, speak=True, tik=False,
        stream=None):
    """
    @param key:   name of stop watch
    @param stage: name of measuring point
    @param pause: whether tok cuts out the time consumed in its proper body 
    @param speak: print out measured delta
    @param mcs:   show microseconds  
    @param tik:   just set start point, no rpint out, no delta kept
    @return: seconds since last tok or tik on this key
    
    * Measurements are also kept in tok.history.  
    * tok is only +-5 microsecond accourat)
    * if key is None last key will be used, if there is no last key
      key is set to empty string (key None cannot be accessed)
       
    """
    new_tick               = time.time()

    ## 
    
    if not 'clocks' in dir(tok):
        if key is None:
            key = ''
        tok.clocks = {}
        tok.last_key = key
        tok.history = {}

    if key is None:
        key = tok.last_key
             
    if key in tok.clocks:
        delta                    = new_tick - tok.clocks[key]
    else:
        tok.last_key = key
        delta = 0.
        tok.history[key] = {}
    
    time_string = timedeltastr(delta,  mcs=mcs)
    
    tok_string = 'tok'
    
    if stage:
        if key:
            tok_string += ' ' + str(key) + '/' + str(stage) 
        else:
            tok_string += ' ' + str(stage)
        if stage:
            tok.history[key][stage] = delta

    else:
        if key:
            tok_string += ' ' + str(key)
        if not tik:
            tok.history[key][len(list(tok.history[key].keys()))+1] = delta

    tok_string += ': ' + time_string 
    
    if speak and not tik:
        if stream is None:
            stream = sys.stdout
        stream.write(tok_string + '\n')
        stream.flush()
        pass
    
    if pause:
        new_tick               = time.time()

    tok.clocks[key]     = new_tick 
        
    return delta

def timedeltastr(delta, mcs=False):
    """@param delta: float of seconds
    
    Return string representation of delta of form ..h ..m ..s ..ms [..mcs]
    """ 
    
    timetuple  = time.localtime(delta)

    hours    = int(delta // 3600)    
    seconds = timetuple[5]
    minutes = timetuple[4]
    milliseconds = int( ( delta * 1000 ) % 1000 )   
    
    if mcs:
        microseconds = int( ( delta * 1000000) % 1000 )   
    
        out = '{:3}'.format(microseconds) + 'mcs'
    else:
        out = ''
    
    if milliseconds or not mcs:
        out = '{:3}'.format(milliseconds) + 'ms ' + out
    if seconds:
        out = '{:2}'.format(seconds) + 's ' + out
    if minutes:
        out = '{:2}'.format(minutes) + 'm ' + out
    if hours:
        out = str(hours)   + 'h ' + out
        
    return out


def timestamp():
    date_time_raw = str(datetime.datetime.now())
    date_time     = re.sub(r'[:\.\ -]','_', date_time_raw )
    return date_time

def timestemp2():
    return ':'.join( [ '%02d' % x  for x in time.localtime()[:6] ] ) 



def stems_from(an_object, class_B):
    """Wether the class of an_object is class_B or inherits from class_B.""" 
    
    ## to avoid confusion we forbid type-objects here
    assert not type(an_object) == type, \
    ( "Please provide non-type object as first arg"
      " or use stem_from_2" )
    return stems_from_2(an_object, class_B)


def stems_from_2(x, class_B):
    """Wether class x or class of object x equals class_B or inherits from class_B.
    The latter is tested if x is a type itself.""" 
    
    if type(x) == type:
        class_A = x
    else:
        class_A = type(x)
    
    bases = list(class_A.__mro__)
    bases.append(class_A)
    bases_   = [ ( c.__module__, c.__name__) for c in bases ]
    class_A_ = (class_B.__module__, class_B.__name__)
    
    return class_A_ in bases_

def is_overwrite(func_name, class_B, class_A):
    """Check whether class_B inherits the method named func_name 
    from class_A or overwrites it"""
    assert hasattr(class_B, func_name)
    assert hasattr(class_A, func_name)
    return not getattr(class_B, func_name) == getattr(class_A, func_name)
        


def module_name(full=False):
    """Return caller module name"""
    parent_frame  = inspect.stack()[1][0]
    path          = parent_frame.f_code.co_filename
    base          = os.path.basename(path)
    module_name   = os.path.splitext(base)[0]
    if full:
        return path
    else:
        return str(module_name)

def get_args(f):
    """Assumes that f is instance method and returns the names of the
    mandatory args --excluding 'self'--, those of optionals args, 
    and a dict that maps optional args to default values."""         
    
    all_varnames    = f.__code__.co_varnames
    all_arguments   = all_varnames[:f.__code__.co_argcount]
    defaults        = f.__defaults__
    n_optional      = len(defaults) if defaults else 0 
    n_mandatory     = len(all_arguments) - n_optional   # including 'self'
    mandatory_names = all_arguments[1:n_mandatory]      # exclude 'self'
    optionals_names = all_arguments[n_mandatory:]
    
    
    if defaults:
        defaults = deepcopy(defaults)
        default_dict = dict( list(zip( optionals_names, defaults)))
    else:
        default_dict = {}
    
    
    #print self, mandatory_names, defaults, default_dict
    return mandatory_names, optionals_names, default_dict


def is_function(self, f):
    raise NotImplementedError()
    inst = None
    cls  = None
    return type(f) in [
        type(lambda: None),
        type(inst.is_function),
        type(cls._container) ]
                
def is_string_like(x):
    """checks wether x is string-like"""
    return  isinstance(x,str) 

def iscontainer(x):
    """checks wether x is container but not string-like"""
    return  not is_string_like(x) and \
            isinstance(x, Container) 



def value2string(value,strf=str):
    try:   
        name = value.get_name()
    except:
        if   type(value) == type(""):
            name =  value
        elif type(value) in [type(0),type(0.)]:
            name = strf(value)
        elif type(value) == type(lambda x:x):
            name = value.__name__
        elif type(value) in  [ type([]),type((None,)) ]:
            name = strf( [ value2string(v,strf=(lambda x:x)) 
                           for v in value ] )
        elif type(value) == type(object):
            name = value.__name__
        else:
            name =  strf(value)
    
    return name



def type_down(o, 
            ids=dict(), 
            add_type=True, 
            recursion_limit=4, 
            drop=[],
            drop_empty=False,
            try_yaml=True, 
            keys_only=[], 
            key='',
            ):
    
    
    
    def drop_match(k):
        drop_it = False
        for pattern in drop:
            if fnmatch(str(k), pattern):
                drop_it = True
                break
        return drop_it
    
    def drop_empty_hit(v):
        if drop_empty:
            try:
                return not bool(v)
            except:
                return False
        else:
            return False
        
    def dictize_(o, key=key):
        return type_down(o, 
                ids,
                add_type,
                recursion_limit,
                drop,
                drop_empty,
                try_yaml,
                keys_only,
                key)
    
    leave_types           = [ str, str, int, float, bool, type(None)]
    
    if type(o) in leave_types:        
        o_ = o
    else:
        if not id(o) in ids:
            ids[id(o)] = 0
            
        if ids[id(o)] > recursion_limit:
                o_ =  str(o)
        else:
            ids[id(o)] += 1
            try:
                o_ = OrderedDict()
                for k in o:                    
                    if not drop_match(k):
                        o__ = dictize_(o[k],str(k))
                        if not drop_empty_hit(o__):
                            o_[str(k)] = o__
            except:
                try:
                    o_ = []
                    for v in o:
                        o_ += [ dictize_(v) ] 
                except:
                    try:
                        
                        o_ = OrderedDict()
                        o_tmp =  OrderedDict(o.__dict__)
                        for k in o_tmp:
                            if not drop_match(k):
                                o__ =  dictize_(o_tmp[k])
                                if not drop_empty_hit(o__):
                                    o_[k] = o__
                        
                    except:
                        o_ = repr(o)
            if type(o_) == OrderedDict:
                if key in keys_only:
                    o_ = list(o_.keys())
            
        assert type(o_) in [str, list, dict, OrderedDict], type(o_)
        
    if type(o_) is dict and add_type:
        done = False
        key = 'TYPE'
        while not done:
            if not key in o_:
                o_[key] = repr(type(o))
                done = True
            else:
                key += key
    
    if try_yaml:
        try:
            to_yaml(o_)
        except:
            m = "wont be able yaml this:\n%s, %s\n" % (str(o_), type(o_))
            raise Exception(m)  

        
    assert type(o_) in ([list, dict, OrderedDict] + leave_types ), type(o_)
                  
    return o_
   

def to_yaml_safe(o,
                 drop=[],
                 keys_only=[],
                 add_type=False,
                 recursion_limit=4,
                 raise_early=True,
                 drop_empty=False,
                 ):
    """make yamlout of object o using type_down. 
    You can filter out mapping keys by adding them to drop, or reduce 
    mappings to key-lists by adding the refrering key from the parent mapping 
    to keys_only 
    TODO: try jsonpickle
    
    """ 
    
    o_ = type_down(o,
                 ids=dict(),
                 add_type=add_type,
                 recursion_limit=recursion_limit,
                 drop=drop,
                 try_yaml=raise_early,
                 keys_only=keys_only,
                 drop_empty=drop_empty
                 )
    
    return to_yaml(o_)
         

def printy(value):
    
    value_str = to_yaml_safe(value)
    
    if re.match(r'\S+.*', value_str):
        value_str = re.sub(r'^[\n\t]+(.*)', r'\1', value_str)
        
    #value_str = '\n' + value_str
       
        
    print(value_str)
    


def to_yaml(value,):
            
    ## credits to coldfix 
    class OrderedDumper(yaml.SafeDumper):
        pass
    def _dict_representer(dumper, data):
        return dumper.represent_mapping(
            yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
            list(data.items()))

    def _pipystr_representer(dumper, data):
        
        if len(data) > 32 and re.search(r'\s', data):    
            style = u'|'
        else:
            style = None
            
        #if not isinstance(data, unicode):
        #    data = unicode(data, 'utf8') ## 'ascii' 'utf8'
        tag = u'tag:yaml.org,2002:str'
        return dumper.represent_scalar(tag, data, style=style)
    
    
    OrderedDumper.add_representer(OrderedDict, _dict_representer)
    OrderedDumper.add_representer(str, _pipystr_representer, )
    
    ## no, this makes quotation around keys
    #OrderedDumper.add_representer(str, _pipystr_representer, )
                                          
    s = yaml.dump(value, 
                  None, 
                  #Dumper,
                  OrderedDumper, 
                  default_flow_style=False, 
                  encoding='utf-8',
                  allow_unicode=True,
                  default_style='',
                  ).strip()
    
    
    return s.decode()


def xml_to_object(node, 
                  mode='dict', 
                  sparse=True, 
                  strip=True,
                  colorful=False):
    assert mode in ['list', 'dict']
    
    tag         = node.tag
    if colorful:
        tag = paint(tag, 'red')
    attributes = node.attrib
    
    text        = str(node.text)
    if text and strip:
        text = text.strip()
    
    tail        = node.tail
    if tail and strip:
        tail = tail.strip()
    
    children    = [ xml_to_object(node_, mode, sparse, strip, colorful) for node_ in node ]
    
    
    if mode == 'dict':
        obj   = OrderedDict()
        
        obj['tag']  = tag
        
        if attributes or not sparse:
            obj['attr'] = OrderedDict(attributes)
        
        if text or not sparse:
            obj['text']       = text

        if tail or not sparse:
            obj['tail']       = tail

        if children or not sparse:
            obj['subs']   = children
        
    if mode == 'list':
        obj   = []
        obj.append(tag)
        if attributes or not sparse:            
            obj.append(attributes)
        if text or not sparse:
            obj.append(text)
        if tail or not sparse:
            obj.append(tail)
        if children or not sparse:
            obj.append(children)
        
    
    return obj
    
def xml_to_object_2(node, 
                    sparse=True, 
                    strip=True,
                    ):
    tag         = node.tag
    attributes = node.attrib    
    text        = node.text
    if text and strip:
        text = str(text).strip()
    tail        = node.tail
    if tail and strip:
        tail = str(tail).strip()
    
    children    = [ xml_to_object_2(node_, sparse, strip) for node_ in node ]
    
    
    root = OrderedDict()
    
    obj = \
    root[tag]  = OrderedDict()
    
    if attributes or not sparse:
        obj['_attr'] = OrderedDict(attributes)
    
    if text or not sparse:
        obj['_text']       = text

    if tail or not sparse:
        obj['_tail']       = tail

    if children or not sparse:
        obj['children']   = children
        

    return obj
    

def xml_to_yaml(xml,  **options):
    from lxml import etree
    root = etree.fromstring(xml)
    root_obj = xml_to_object(root, **options)
    out = to_yaml(root_obj)
    #node = root.find(".//name")   
    #node.text = 'text'
    #node.attrib['a'] = 'A'
    return out
    

def show(x):
    print((to_yaml_safe(x)))



def dump_anydict_as_map( anydict):
    yaml.add_representer( anydict, BaseRepresenter.represent_mapping)
    
def _represent_dictorder( self, data):
    return self.represent_mapping('tag:yaml.org,2002:map', list(data.items()) )

class Loader_map_as_anydict( object):
    'inherit + Loader'
    anydict = None      #override
    @classmethod        #and call this
    def load_map_as_anydict( klas):
        yaml.add_constructor( 'tag:yaml.org,2002:map', klas.construct_yaml_map)

    'copied from constructor.BaseConstructor, replacing {} with self.anydict()'
    def construct_mapping(self, node, deep=False):
        if not isinstance(node, MappingNode):
            raise ConstructorError(None, None,
                    "expected a mapping node, but found %s" % node.id,
                    node.start_mark)
        mapping = self.anydict()
        for key_node, value_node in node.value:
            key = self.construct_object(key_node, deep=deep)
            try:
                hash(key)
            except TypeError as exc:
                raise ConstructorError("while constructing a mapping", node.start_mark,
                        "found unacceptable key (%s)" % exc, key_node.start_mark)
            value = self.construct_object(value_node, deep=deep)
            mapping[key] = value
        return mapping

    def construct_yaml_map( self, node):
        data = self.anydict()
        yield data
        value = self.construct_mapping(node)
        data.update(value)


class Loader( Loader_map_as_anydict, yaml.Loader):
    anydict = OrderedDict
    
def from_yaml(ystring, ordered_dict=True):
    #return yaml.load(ystring, Loader=yaml.BaseLoader)
    
    ## thanks to coldfix 
    if ordered_dict:
        class OrderedLoader(Loader):
            pass
        def construct_mapping(loader, node):
            loader.flatten_mapping(node)
            return OrderedDict(loader.construct_pairs(node))
        OrderedLoader.add_constructor(
            yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
            construct_mapping)
        
        return yaml.load(ystring, OrderedLoader)
        
    return yaml.load(ystring)


def from_yaml_file(path):
    h = open(path,mode='r')
    content = h.read()
    h.close()
    return from_yaml(content)
    






def paint(text, color, mode='normal'):
    
    color_map = {
        'red':      31,
        'green':    32,
        'yellow':   33,
                  }
    
    mode_map = {
        'normal': 0
        }
    out =  '\033[{};{}m'.format(mode_map[mode], color_map[color]) + \
            text + '\033[0m'

    return out 

def str2bool(s):
    
    s = str(s)
    
    peeled = True
    while peeled:
        peeled = False  
        if len(s) > 1:
            if   s[0] == "'" and s[-1] == "'":
                s = s[1:-1]
                peeled = True
            if s[0] == '"' and s[-1] == '"':
                s = s[1:-1]
                peeled = True
                
    
    custom_trues  = ['true', 'yes', 'y', 'si', '1', 'ja', 'on', 'aber hallo',
                     'normal']
    
    custom_falses = [ 'false','no', 'n', 'nein', 'off', 'lieber nicht', 
                      'nix da', '0', 'gehts noch?' ,'']
    
    
    
    sls = s.strip().lower()
    
    if sls in custom_trues:
        return True
    
    if sls in custom_falses:
        return False
     
    try:
        return bool(float(s))
    except:
        pass
    
    try:
        return bool(s)
    except:
        pass
    
    return False


    
    
def discover_classes(dirs, ancestor=None,name=None, first_only=False):
    """non-recursive class discovery"""
    dirs = [ os.path.normpath(os.path.abspath(d)) for d in dirs ]
    
    hits = []
    for ddir in dirs:
        
        if os.path.isdir(ddir):
            #print(ddir)
            
            spath_appended = False
            if ddir not in sys.path:
                spath_appended = True
                sys.path.append(ddir)
            module_paths  = glob.glob(os.path.join(ddir, '*.py'))
            
            #print(module_paths)
            for module_path in module_paths:
                hits_ = classes_from_module(module_path, 
                                            ancestor, 
                                            name,
                                            first_only)
                hits += hits_
                if hits and first_only:
                    break
    
                #print(hits_)
            if spath_appended:
                sys.path.remove(ddir)
        else:
            message = 'cannot find dir {}'.format(ddir)
            raise Exception(message)
        if hits and first_only:
            break
    
        
    return hits
    

def classes_from_module(module_path, 
                        ancestor=None, 
                        name=None, 
                        first_only=False):
    
    hits = []
    module_stem,_ = os.path.splitext(module_path)
    module_name   = os.path.basename(module_stem)    
    
    module = imp.load_source(module_name, module_path)
    for _,a in module.__dict__.items():
        if type(a) == type:
            if ancestor is None or issubclass(a, ancestor):
                if name is None or a.__name__ == name:
                    hits += [a]
                    if first_only:
                        break
    
            
    return hits

if __name__ == '__main__':
    
    pass
