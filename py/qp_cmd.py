"""DNode command line tool"""
import os, re, sys, collections, glob
from collections import OrderedDict

from cut.fileaccess  import homedir
from cut.fileaccess import save_file, read_file
from cut.mixedtools import from_yaml, shelloex, to_yaml
from scmd.cmd_line import CmdLine 

from qp import make_plot, list_countries, list_country_groups, make_plots

 
class qplotCmd(CmdLine):
    
    def __init__(   self,
                    prog               = "qplot",
                    version            = "0.1",             
                    base_dir_distance  = 2,
                    **args
                ):
        
        super().__init__(   prog=prog,
                            version=version,
                            base_dir_distance=base_dir_distance,
                            **args
                        )

    def build_parser(self):
    
        ## main args
        self.update_argument('--verboseness',       '-v')
        self.add_arguments_main(
            #'--dry-run', 
            )
        ## subcmd args        
        self.define_argument('country',         
                             help='key of root node',
                             default = "Germany",
                             choices = self.list_countries(),
                             completion_func=self.list_countries
                             )
    
        self.define_argument(   '--remove-previous-plots', '-P',  
                                help='remove all existing files from plot-dir',
                                action='store_true'
                                )
        self.define_argument(   '--show', '-s',  
                                help='open ploted graph with xviewer',
                                action='store_true'
                                )
        self.define_argument(   '--plot-viewer', '-x',  
                                help='executable to view plot-png-files',
                                default='xviewer'
                                )


        ## 
        self.add_subcmd('plot', 
                        help='create plot-file for one country',
                        func=self.plot,
                        arguments = (   'country',
                                        '--show',
                                         '--plot-viewer',
                                        '--remove-previous-plots')
                                        )
        ##
        self.define_argument(   'what',         
                                help='what to list',
                                choices = ['countries']
                                #completion_func=...
                                )

        ## 
        self.add_subcmd('list', 
                        help      = 'list something',
                        func      = self.list,
                        arguments = ('what',
                                    )
        )
        
    def plot(self,args):
        
        if args.remove_previous_plots:
            plot_path_pattern = os.path.join(self.base_dir, 'plots', '*.png')
            print(plot_path_pattern)
            plot_paths = glob.glob(plot_path_pattern)
            for path in plot_paths:
                #print(path)
                os.remove(path)

        if args.country in list_country_groups():
            group = args.country
            paths = make_plots(group)
        else:


            paths = [make_plot(country=args.country)]
        
        if args.show:
            for path in paths:  
                shelloex([args.plot_viewer, path], bg=True)

    
    def list(self,args):
        if args.what == 'countries':
            countries = self.list_countries()
            self.say(to_yaml(countries))

    
    def list_groups(self):
        return list(list_country_groups())

    def list_countries(self):
        return list(list_countries()) + self.list_groups()

    def run(self, args):
        
        return args.func(args)

        
        
