#!/bin/python3
import sys
from pathlib import Path
import matplotlib
import csv

from cut.numpytools import *
from cut.fileaccess import *
from cut.mixedtools import to_yaml
from syspath import get_base_dir

from pylab import *
from matplotlib.dates import YearLocator, MonthLocator, DateFormatter
import datetime
import matplotlib.pyplot as plt


## print config-file location
#print(matplotlib.matplotlib_fname())



def get_best_major_locator(dates):
    months   = MonthLocator()  # every month
    days     = DayLocator()    
    years    = YearLocator()
    
    return days
    
def get_best_major_formatter(dates):
    
    return DateFormatter('%y')



datasets = {
    'covid19_jh_confirmed': {
        'path': get_base_dir() + '/data/time_series_19-covid-Confirmed.csv'
        },
    'covid19_jh_deaths': {
        'path': get_base_dir() + '/data/time_series_19-covid-Deaths.csv'
        },
        

             
         }


def read_lines(data_set_key):

    with open(datasets[data_set_key]['path'], mode='r') as csv_file:
        csv_reader = csv.reader(csv_file)
        return list(csv_reader)
        

def parse_lines_covid19_jh(lines):

    confirmed_d = OrderedDict()
    dates_raw     = None
    for line in lines:
        if dates_raw:
            if not line[0]:
                country = line[1]  
                confirmed_d[country] = [ float(s) if s else 0. for s in line[4:] ]
        else:
            dates_raw = line[4:]

    dates = []
    for date_raw in dates_raw:
        date = datetime.datetime.strptime(date_raw, r'%m/%d/%y')
        dates += [date]
 
    dates = date2num(dates)

    return dates, confirmed_d

def format_date(x, pos=None):
    dt = num2date(x) 
    #print(dt.isoweekday())
    return  datetime.datetime.strftime(dt, ('sunday ' if dt.isoweekday() == 7 else '' ) + r'%m-%d')


def delta_1(dates, values):
    dates  = dates[1:]
    values = [ values[i] - values[i-1] for i in range(1,len(values))  ]
    return dates, values

def last(dates, values, n):
    dates   = dates[-n:]
    values  = values[-n:]
    return dates, values

def list_countries():

    lines_confirmed = read_lines('covid19_jh_confirmed')[:]
    _, confirmed  = parse_lines_covid19_jh(lines_confirmed)
    return confirmed.keys()


country_groups = {
    "group_00": [
        'Germany', 'France', 'Spain', 'Italy', 'US', 'Netherlands',  'Switzerland', 'Sweden',
        "United Kingdom", "Belgium"]
}

def list_country_groups():
    return country_groups.keys()



def make_plots( group,
                n       = 20,
                mode    = 'increase'
                ):
    paths = []
    for country in country_groups[group]:
        path = make_plot(country, n=n, mode=mode)
        paths.append(append)
    return paths

def make_plot(country="Germany", 
         n=20,
         mode    = 'increase'
        ):


    name   = country + '-' + mode

    lines_confirmed = read_lines('covid19_jh_confirmed')[:]
    lines_deaths    = read_lines('covid19_jh_deaths')[:]
    
    dates, confirmed  = parse_lines_covid19_jh(lines_confirmed)
    dates, deaths     = parse_lines_covid19_jh(lines_deaths)
    
   
    values_c = confirmed[country]
    values_d = deaths[country]
    if mode == 'increase':
        dates, values_c = delta_1(dates, values_c)
        dates, values_d = delta_1(dates, values_d)

    dates, values_c = last( dates, values_c, n)
    dates, values_d = last( dates, values_d, n)

 
    fig, ax1 = plt.subplots(ncols=1, figsize=(8, 6))
    ax1.plot(dates, values_c, 'o-', markersize=6,)
    ax1.xaxis.set_major_formatter(
        #matplotlib.dates.DateFormatter( r'%m-%d')
        matplotlib.ticker.FuncFormatter(format_date)
        )
    ax1.xaxis.set_major_locator(get_best_major_locator(dates))
    ax1.grid(True)
    ax1.set_ylabel('confirmed', 
    #color='blue'
    )

    
    if mode == 'increase':

        text_add =  "total confirmed: %d\ntotal deaths: %d" % \
                    (confirmed[country][-1], deaths[country][-1])    

        props = dict(#boxstyle='round', 
                    facecolor='white', 
                    #alpha=0.5
                    )

        ax1.text(0.04, 0.8, text_add, 
                transform=ax1.transAxes, 
                fontsize=9,
                verticalalignment='top', 
                bbox=props
                )

   

    ax2 = ax1.twinx() 
    ax2.set_ylabel('deaths', color='black')
    ax2.set_ylim(0,max(values_d)*2)
    ax2.plot(dates, values_d, '+-', color='black', markersize=6, )

    fig.autofmt_xdate()

    title(name)
    name_ = re.sub(r'\s','_', name)
    path = os.path.join(get_base_dir(), 'plots', name_ +'.png')
    print("save plot in")
    print(path)
    fig.savefig(path)

    return path
 

if __name__ == '__main__': 
    
    #main()
    #plot_by_date()
    pass
    
    
