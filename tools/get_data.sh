#!/bin/bash
BASEDIR=$(cd $(dirname $BASH_SOURCE)/..; pwd)

SOURCE1=https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv
SOURCE2=https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv


DEST1=$BASEDIR/data/time_series_19-covid-Confirmed.csv
DEST2=$BASEDIR/data/time_series_19-covid-Deaths.csv


echo $SOURCE1
echo $SOURCE2

curl -o $DEST1 "$SOURCE1"
curl -o $DEST2 "$SOURCE2"
